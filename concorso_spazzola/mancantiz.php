<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        table {
            border-collapse: collapse;
            border-spacing: 0;
            width: 100%;
            border: 1px solid #ddd;
        }

        th, td {
            text-align: left;
            padding: 16px;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2
        }
    </style>
</head>
<body>

<?php

require_once 'd_b.php';
/*
function array2csv(array &$array)
{
    if (count($array) == 0) {
        return null;
    }
    ob_start();
    $df = fopen("php://output", 'w');
    fputcsv($df, array_keys(reset($array)));
    foreach ($array as $row) {
        fputcsv($df, $row);
    }
    fclose($df);
    return ob_get_clean();
}
*/

echo '<table>
  <tr>
    <th>N</th>
    <th>ID FORM</th>
    <th>EMAIL</th>
    <th>COGNOME</th>
    <th>CARTA IDENTITA\'</th>
    <th>CARTA IDENTITA\' (retro)</th>
    <th>FORM</th>
  </tr>';
$lista = listaAllMancanti($conn);

fputcsv($df, $nome_colonne);
$numeri = 0;
foreach ($lista as $elemento) {
    $numeri++;
    $row = unserialize($elemento['dati_excel']);

    
    $link_form = "http://www.franckprovost-expert.it/concorso-franck-provost-ti-regala-la-professionalita_recall.php?token=" . base64_encode($elemento['email']);

    echo '<tr>
    <td>' . $numeri . '</td>
    <td>' . $elemento['id_form'] . '</td>
    <td>' . $elemento['email'] . '</td>
    <td>' . $row[8] . '</td>
    <td><a href="' . $row[6] . '" target="_blank">link</a> </td>
    <td><a href="' . $row[7] . '" target="_blank">link</a> </td>
    <td > <a href="' . $link_form . '" target="_blank">link</a></td>
  </tr > ';

}
echo '</table>';
?>


</body>
</html>