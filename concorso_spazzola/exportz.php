<?php

require_once 'd_b.php';
/*
function array2csv(array &$array)
{
    if (count($array) == 0) {
        return null;
    }
    ob_start();
    $df = fopen("php://output", 'w');
    fputcsv($df, array_keys(reset($array)));
    foreach ($array as $row) {
        fputcsv($df, $row);
    }
    fclose($df);
    return ob_get_clean();
}
*/
function download_send_headers($filename)
{
    // disable caching
    $now = gmdate("D, d M Y H:i:s");
    header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
    header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
    header("Last-Modified: {$now} GMT");

    // force download
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");

    // disposition / encoding on response body
    header("Content-Disposition: attachment;filename={$filename}");
    header("Content-Transfer-Encoding: binary");
}

download_send_headers("data_export_" . date("Y-m-d_H:i:s") . ".csv");
$lista = listaAll($conn);
/*echo count($lista);
foreach ($lista as $elemento){
    var_dump(unserialize($elemento['dati_excel'])); break;

}die();*/
$nome_colonne = ['#',
    'ID',
    'email',
    'foto',
    'data e ora',
    'foto-carta-identità-fronte',
    'foto-carta-identita-retro',
    'cognome',
    'nome',
    'provincia',
    'cap',
    'cellulare',
    'data-di-nascita-gg',
    'data-di-nascita-mm',
    'data-di-nascita-aaaa',
    'ora-erogazione-scontrino-hh',
    'ora-erogazione-scontrino-mm',
    'totale-spesa',
    'totale-spesa-intera',
    'totale-spesa-decimale',
    'numero-scontrino',
    'informativa',
    'profilazione',
    'marketing',
    'indirizzo',
    'citta',
    'data-erogazione-scontrino-gg',
    'data-erogazione-scontrino-mm',
    'data-erogazione-scontrino-aaaa',
    'link-form',
];

ob_start();
$df = fopen("php://output", 'w');
fputcsv($df, $nome_colonne);
$cerca=array("â¬","â");
$sostituisci=array("€","''");
foreach ($lista as $elemento) {
    $row = unserialize($elemento['dati_excel']);

    $row_due=array();
    $n=1;
    foreach ($row as $colon){
        if($n==9) {
            // aggiungo il nome
            $row_due[] = ($elemento['nome'] != 'no-name') ? " ".utf8_encode($elemento['nome']) : "--";
            // inserisco il valore della colonna 9
            $row_due[] = str_replace($cerca, $sostituisci, utf8_encode($colon))." ";

        }else{

            $row_due[]=str_replace($cerca,$sostituisci,utf8_encode((string)$colon))." ";


        }

        $n++;
    }
    $row_due[]="http://www.franckprovost-expert.it/concorso-franck-provost-ti-regala-la-professionalita_recall.php?token=".base64_encode($elemento['email']);
    fputcsv($df, $row_due);

}
fclose($df);
echo ob_get_clean();
die();