<?php
require_once 'd_b.php';
header('Content-type: application/json');

controllo($conn);


/*
foreach ($_POST as $key => $value){
    echo "Field ".htmlspecialchars($key)." is ".htmlspecialchars($value)."<br>";



}*/
//$array=[["error"]=>1, ["errorCodes"]=>[13], ["missing"]=>[], ["typeError"]=>[0=>["nome"]]];
/*$array["error"]=1;
$array["errorCodes"]=[13];
$array["missing"]=array();
$array["typeError"]=array("nome");



echo json_encode($array);*/
/*
array(4) {
    ["error"]=> int(1)
    ["errorCodes"]=> array(2) {[0]=> int(13) [1]=> int(4) }
    ["missing"]=> array(0) { }
    ["typeError"]=> array(1) { [0]=>
        array(2) {
        [0]=> string(25) "foto-carta-identita-retro"
        [1]=> string(4) "foto" }
    }
}*/


function controllo($conn)
{
    $nome = "";
    $token = "";

    if (isset($_POST['informativa']) && $_POST['informativa'] == "no") {
        // utente non trovato
        $array["error"] = 1;
        $array["errorCodes"] = [105];
        $array["missing"] = array();
        $array["typeError"] = array("informativa");


        echo json_encode($array);
        return;
    }

    if (isset($_POST['nome']) && $_POST['nome'] != "") {
        $nome = $conn->real_escape_string(trim($_POST['nome']));


    } else {
        // errore nome
        $array["error"] = 1;
        $array["errorCodes"] = [100];
        $array["missing"] = array();
        $array["typeError"] = array("nome");

        echo json_encode($array);
        return;
    }


    if (isset($_POST['token'])) {
        $token = base64_decode($conn->real_escape_string($_POST['token']));

    } else {
        // token assente
        $array["error"] = 1;
        $array["errorCodes"] = [101];
        $array["missing"] = array();
        $array["typeError"] = array("token");


        echo json_encode($array);
        return;
    }

    $utente = finduser($token, $conn);

    if ($utente === false) {
        // utente non trovato
        $array["error"] = 1;
        $array["errorCodes"] = [102];
        $array["missing"] = array();
        $array["typeError"] = array("utente");


        echo json_encode($array);
        return;
    }

    if ($utente['nome'] != 'no-name') {
        // utente già aggiornato
        $array["error"] = 1;
        $array["errorCodes"] = [103];
        $array["missing"] = array();
        $array["typeError"] = array("utente");


        echo json_encode($array);
        return;
    }


    $query = "UPDATE concorso_professionalita_finale
              SET nome='" . $nome . "'
              WHERE email='" . $token . "'  ";

    if ($conn->query($query) === TRUE) {

        $array["error"] = 0;
        $array["errorCodes"] = array();
        $array["missing"] = array();
        $array["typeError"] = array();

        echo json_encode($array);
    } else {
        $array["error"] = 1;
        $array["errorCodes"] = [104];
        $array["missing"] = array();
        $array["typeError"] = array("db-error");


        echo json_encode($array);
        return;
    }


}
