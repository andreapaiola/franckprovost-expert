<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>FRANCK PROVOST</title>
<meta name="description" content="DESCRIPTION" />
<meta name="keywords" content="KEYWORDS" />
<link href="../css/style.css" rel="stylesheet" type="text/css"><!-- Shadowbox -->
<link rel="stylesheet" type="text/css" href="store-locator.css">

<script type="text/javascript" src="/js/jquery-1.7.1.min.js"></script>

<!-- Shadowbox -->
<link rel="stylesheet" type="text/css" href="../js/shadowbox-3.0.3/shadowbox.css">
<script type="text/javascript" src="/js/shadowbox-3.0.3/shadowbox.js"></script>
<script type="text/javascript">
	Shadowbox.init({	displayCounter:	false});
	window.onload = function(){
	  Shadowbox.setup(document.getElementById('mappa').getElementsByTagName('area'));
	};
</script>

<!--dropdown-->
<script type="text/javascript" src="../js/dropdown/bootstrap-dropdown.js"></script>
<link rel="stylesheet" type="text/css" href="../js/dropdown/dropdown-menu.css">

</head>

<body id="inside">

<div class="marchio">

<div class="header_cont">
	<div class="header">
		<div class="logo"><img src="/img/img_logo.png" alt="Logo" border="0" title="Logo" /></div>
		<ul class="menu">
			<li><a href="../default.htm" >Homepage</a></li>
			<li ><a href="../marchio.htm"  id="evid" >La Marca</a></li>					
			<li><a href="../consigli.htm">I miei consigli</a></li>
			<li class="dropdown" ><a class="dropdown-toggle" data-toggle="dropdown" href="#menu1">La Gamma</a>
				<ul class="dropdown-menu">
					<li><a href="../gamma-colorazione.htm">Colorazione</a></li>
					<li><a href="../gamma_couleur.htm">Hair Care</a></li>
					<li><a href="../gamma-styling-mousse.htm">Styling</a></li>
				</ul>
			</li>
			<li><a href="../store-locator/store-locator.php" id="no_border">I punti vendita</a></li>
		</ul>
	</div> <!--header-->
</div> <!--header_cont-->

<div id="navbar">
<p><a href="../default.htm">Homepage</a> &raquo; <a
	href="store-locator/store-locator.php">I punti vendita</a></p>
</div>

<div class="main_content">

<h1>I PUNTI VENDITA FRANCK PROVOST</h1>


<div class="content">


<div class="searchWrap-locator">
	<div class="mapItaly-locator">
		<img src="img/italia.png" />
	</div>
<div class="search-locator">
	<form action="../store-locator/search.php" method="post" id="searchForm">
		<div class="form-entry">
			<label for="region" style=>Seleziona una regione</label>
			<select id="region" class="store-select">
			</select>
		</div>
	<div class="form-entry">
		<label for="province">Seleziona una provincia</label>
		<select id="province" class="store-select">
		</select>
	</div>
	<div class="form-entry">
		<label for="town">Seleziona un comune</label>
		<select id="town" class="store-select">
		</select>
	</div>
	<input type="submit" name="submit" value="CERCA" class="cerca">
</form>

</div>

</div>

<!-- end searchWrap -->

<div class="wrapTxt-locator">
<h2>Risultati della ricerca</h2>
<div id="result" style="float: rigth; height: 456px; overflow: auto;">
<div id="result_content">
<ul class="wrap_result">
	<li style="margin: 10px 0 20px 0 !important; padding: 0;">
	<span class="azienda_name" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
	Per ottenere la lista dei punti vendita prima seleziona la Regione, la Provincia, il Comune e infine premi sul tasto Cerca. </span> 
	</li>
</ul>
</div>
</div>
</div>

</div>
</div>

<!-- end content -->


<div style="width:50%;float:right;overflow:hidden;">
<!-- dx --></div>
<!-- content --></div>
<!-- main_content --></div>
<!-- inside -->



<div id="footer_inside">

<div class="footer_menu">
<div class="left">*Parrucchiere N&deg;1 per numero di saloni presenti sul territorio</div>
<ul>
	<li><a rel="shadowbox;width=350;height=400" class="option"
		title="Informazioni legali" href="index.html#legal-box">Informazioni
	legali</a></li>

	<li><a rel="shadowbox;width=350;height=200" class="option"
		title="Contatti" href="../index.html#contatti-box" id="no_border">Contatti</a></li>
</ul>
</div>

</div>



<div id="legal-box" style="display: none">
<div id="legal-box-content" style="padding: 20px">

<p align="center"><strong><u>PRIVACY POLICY</u></strong></p>
<br />
<br />
<p>1. La presente nota informativa &egrave; stata redatta da
L&rsquo;Or&eacute;al Italia S.p.A., con sede in Torino, via Garibaldi
42, (&ldquo;Societ&agrave;&rdquo;) ai sensi dell&rsquo;articolo 13 D.Lgs
196/2003 in materia di protezione dei dati personali (&ldquo;Codice
Privacy&rdquo;), al fine di descrivere le modalit&agrave; di gestione
del sito <a href="http://http://www.provost.it" tagert="_blank"
	style="color: black">http://http://www.provost.it</a> (&ldquo;il
Sito&rdquo;) e dei servizi messi a disposizione, attraverso il Sito,
dalla stessa Societ&agrave;, L&rsquo;Or&eacute;al Italia S.p.A.(&ldquo;i
Servizi&rdquo;), in relazione al trattamento dei dati personali degli
utenti che consultano il Sito e/o fruiscono dei Servizi (&ldquo;gli
Utenti&rdquo;). <br />
<br />
<strong>RACCOLTA DEI DATI PERSONALI</strong> <br />
<br />
<u>Registrazione alla community</u> <br />
<br />
2. In alcune sezioni del Sito, ovvero in occasione della fruizione dei
Servizi, all&rsquo;Utente viene offerta la possibilit&agrave; di
registrarsi entrando in un&rsquo;apposita area del Sito e beneficiare
cos&igrave; di tutta una serie di vantaggi esclusivi riservati agli
Utenti registrati. <br />
<br />
Il tutto &egrave; gestito dalla Societ&agrave; attraverso la newsletter,
comunicazione periodica inviata, per mezzo della posta elettronica,
all&rsquo;indirizzo e-mail indicato dall&rsquo;Utente, attraverso la
quale gli Utenti registrati saranno informati delle iniziative esclusive
che la Societ&agrave; metter&agrave; a disposizione dei propri Utenti
registrati, quali concorsi a premio, servizi esclusivi, blog, forum,
aree di discussione, inviti etc. (&ldquo;Newsletter&rdquo;). La
Newsletter potr&agrave; anche contenere banner pubblicitari, inserzioni
e offerte pubblicitarie sia della Societ&agrave; che di soggetti terzi.
<br />
<br />
All&rsquo;atto della registrazione, all&rsquo;Utente sar&agrave;
richiesto di inserire dati personali, alcuni dei quali - di volta in
volta espressamente individuati come tali - sar&agrave; obbligatorio ed
indispensabile conferire ai fini della registrazione (es.
l&rsquo;indirizzo e-mail presso il quale ricevere le Newsletter). Gli
altri dati potranno essere o non essere forniti, a discrezione
dell&rsquo;Utente, ed il mancato conferimento di essi non &egrave; in
alcun modo pregiudizievole ai fini dell&rsquo;iscrizione, ma pu&ograve;
rendere impossibile per l&rsquo;Utente la partecipazione ad alcuni
concorsi o iniziative, o comunque consentirgli una fruizione dei Servizi
solo parziale. Laddove richiesto, gli Utenti che non forniranno il
proprio numero di telefono cellulare o il relativo consenso non potranno
ad esempio essere contattati via SMS per essere invitati ad anteprime o
eventi, e non potranno partecipare a quei concorsi, giochi, sondaggi
condotti attraverso SMS, MMS o altra messaggistica mobile. Gli stessi
non potranno, inoltre, ricevere suonerie, loghi, sfondi e altri
contenuti multimediali destinati ai telefoni cellulari, n&eacute;
offerte commerciali, sconti, info su promozioni da parte della
Societ&agrave; e/o dei suoi partner, veicolati attraverso reti e
terminali di telefonia mobile. <br />
<br />
<u>Navigazione del Sito e cookies</u> <br />
<br />
3. Il server del Sito, anche attraverso i cookies, registra
automaticamente alcune informazioni sul computer e sulla navigazione
dell&rsquo;Utente, quali ad esempio: il nome del suo provider di accesso
alla rete Internet, sito di provenienza, pagine visitate, data e durata
della visita, etc. Tali informazioni saranno registrate in forma
anonima, ed utilizzate soltanto per consentire l&rsquo;accesso al Sito
e/o il godimento di alcuni Servizi, e quindi cancellate, oppure
utilizzate in forma aggregata per fini statistici del Sito e per
monitorare, sempre in forma aggregata e mai individuale, quali pagine
del Sito e quali Servizi siano maggiormente graditi dagli Utenti. <br />
<br />
Anche le opinioni e le preferenze espresse dall&rsquo;Utente
nell&rsquo;ambito di un concorso, di un sondaggio o di un&rsquo;altra
similare iniziativa, laddove richieste, vengono trattate dalla
Societ&agrave; soltanto in forma aggregata e mai individuale, n&eacute;
a fini di profilazione dell&rsquo;Utente. <br />
<br />
<u>Dati di soggetti terzi</u> <br />
<br />
4. In alcune aree del Sito pu&ograve; essere fornita all&rsquo;Utente la
possibilit&agrave; di inviare articoli, messaggi, gadget o regali a
soggetti terzi; in tali sezioni pu&ograve; essere richiesto
all&rsquo;Utente di indicare l&rsquo;indirizzo e/o il nome del
destinatario. La Societ&agrave; utilizza tali dati unicamente per dar
corso alla richiesta dell&rsquo;Utente, e poi li cancella immediatamente
dal proprio database, senza farne alcuna ulteriore utilizzazione. <br />
<br />
<u>Cookies</u> <br />
<br />
5. I cookies utilizzati sono dei file di testo che il Sito trasferisce
sul computer dell&rsquo;Utente al fine di facilitarne la navigazione.
Grazie ai cookies, pu&ograve; essere agevolato il riconoscimento
dell&rsquo;Utente registrato da parte del Sito, evitando ad esempio che
si debba procedere alla propria autenticazione (mediante
&ldquo;log-in&rdquo; e &ldquo;password&rdquo;) in occasione di ogni
accesso al Sito. I cookies, inoltre, possono consentire agli Utenti di
personalizzare la propria fruizione del Sito mediante salvataggio delle
proprie impostazioni preferite. <br />
<br />
Come sopra evidenziato, i cookies registrano anche alcune informazioni
relative alla navigazione dell&rsquo;Utente; tali informazioni non
vengono tuttavia registrate in forma individuale, e non interferiscono
pertanto con la privacy e la riservatezza degli Utenti. <br />
<br />
Ciascun utente ha la facolt&agrave; di disattivare i cookies attraverso
il software utilizzato per la consultazione della rete Internet (c.d.
&ldquo;browser&rdquo;). Molti browser sono impostati in modo tale da
accettare i cookies in assenza di diverse istruzioni da parte
dell&rsquo;Utente. La Societ&agrave; invita gli Utenti a verificare le
impostazioni del proprio browser con riferimento ai cookies, e di
regolarle secondo le proprie preferenze, tenendo tuttavia presente che
per usufruire di alcuni Servizi potrebbe essere necessario abilitare la
ricezione dei cookies (in tali casi l&rsquo;Utente verr&agrave;
debitamente informato). <br />
<br />
<strong>MODALIT&Agrave; E FINALIT&Agrave; DEL TRATTAMENTO</strong> <br />
<br />
<u>Conservazione ed accesso ai dati</u> <br />
<br />
6. Nella propria qualit&agrave; di titolare del trattamento, la
Societ&agrave; procede, direttamente o tramite gli eventuali
responsabili indicati nel Sito, a salvare i dati personali degli Utenti
in appositi server e ad effettuare tutte le altre operazioni di
trattamento attraverso il personale - del titolare e del responsabile -
all&rsquo;uopo preposto nella qualit&agrave; di incaricato, ovvero
attraverso eventuali incaricati esterni in occasione di operazioni di
manutenzione. <br />
<br />
Il database &egrave; accessibile soltanto da parte dei soggetti
abilitati mediante modalit&agrave; che ne garantiscono la protezione e
la riservatezza, grazie all&rsquo;adozione di misure di sicurezza
predisposte per prevenire le perdita dei dati, usi illeciti o non
corretti ed accessi non autorizzati. <br />
<br />
I dati vengono conservati per il tempo strettamente necessario per dar
corso a quelle operazioni di trattamento poste in essere in relazione ai
dati di ciascun Utente, a seconda delle sue scelte, preferenze ed
indicazioni. <br />
<br />
<u>Trattamento in generale</u> <br />
<br />
7. La Societ&agrave; utilizza i dati degli Utenti in conformit&agrave;
con le scelte che l&rsquo;Utente ha liberamente esercitato mediante il
proprio consenso, al momento della loro raccolta o successivamente,
oltre che per ottemperare ad obblighi previsti dalla legge, da un
regolamento o da una direttiva comunitaria. Alcuni dati vengono
utilizzati senza richiedere il consenso - ai sensi dell&rsquo;articolo
24 del Codice Privacy - per adempiere ad una specifica richiesta
formulata dall&rsquo;Utente e nei limiti della richiesta stessa. <br />
<br />
<u>Operazioni di trattamento subordinate al consenso, comunicazione e
diffusione dei dati</u> <br />
<br />
8. Soltanto in presenza di un&rsquo;espressa autorizzazione da parte
dell&rsquo;Utente, la Societ&agrave; procede ad utilizzare i suoi dati
per le medesime attivit&agrave; di cui al comma precedente con
l&rsquo;ausilio di strumenti automatizzati senza l&rsquo;intervento di
un operatore e/o per le altre finalit&agrave; di volta in volta
autorizzate dall&rsquo;Utente. Allo stesso modo, solo a fronte
dell&rsquo;espresso consenso dell&rsquo;Utente la Societ&agrave;
potr&agrave; condividere o comunicare i suoi dati ai propri partner
commerciali - di volta in volta espressamente indicati o elencati nel
Sito - affinch&eacute; gli stessi procedano, nella qualit&agrave; di
titolari o responsabili, a separati trattamenti aventi le medesime
modalit&agrave; e gli stessi limiti di cui al trattamento autorizzato
alla Societ&agrave;. I dati degli Utenti, se non previo consenso da
parte degli stessi, non vengono mai diffusi, ad eccezione di quelli che
volontariamente l&rsquo;Utente rende disponibili sulle eventuali aree
comuni del Sito pubblicamente accessibili. La Societ&agrave; invita gli
Utenti a non fornire i propri dati personali all&rsquo;interno delle
aree comuni del Sito pubblicamente accessibili, riservandosi il diritto
di cancellare tali dati o renderli anonimi ove si tratti di dati
sensibili secondo le definizione di cui all&rsquo;articolo 4 comma 1
lettera d) del Codice Privacy. I dati possono, infine, essere comunicati
alla pubblica autorit&agrave; in presenza di un legittimo ordine dalla
stessa impartito. <br />
<br />
<strong>DIRITTI DEGLI UTENTI</strong> <br />
<br />
9. In conformit&agrave; con quanto previsto dall&rsquo;articolo 7 del
Codice Privacy, la Societ&agrave; concede all&rsquo;Utente la
facolt&agrave; di modificare in qualsiasi momento le proprie preferenze
in ordine al trattamento dei dati personali che lo riguardano, e di
ottenere la conferma dell&rsquo;esistenza o meno di tali dati,
nonch&eacute; di conoscerne la provenienza, il contenuto, le
finalit&agrave; e le modalit&agrave; del trattamento, di chiederne
l&rsquo;aggiornamento, la rettificazione, l&rsquo;integrazione, oppure
di pretendere la cancellazione o il blocco di quelli trattati in
violazione di legge. <br />
<br />
L&rsquo;Utente ha, peraltro, il diritto di opporsi al trattamento dei
suoi dati, cos&igrave; come di richiedere in qualsiasi momento la
cessazione del trattamento a fini di ricerche di mercato, invio di
materiale pubblicitario, vendita diretta, comunicazione commerciale. <br />
<br />
<strong>I diritti appena elencati possono essere esercitati scrivendo al
Responsabile del trattamento: DM Group S.p.A., con sede in Torino, Via
Lessolo 19- 10100.</strong> <br />
<br />
<strong>UTENTI MINORENNI</strong> <br />
<br />
10. La Societ&agrave; &egrave; consapevole che il Sito ed i Servizi
possono interessare un pubblico minorenne. <br />
<br />
Per quanto riguarda la raccolta ed il trattamento di dati personali, la
Societ&agrave; non compie alcuna operazione di trattamento dei dati
personali di minorenni senza il consenso dei loro genitori. <br />
<br />
La registrazione &egrave; pertanto consentita ai soli Utenti maggiorenni
o agli Utenti che abbiamo compiuto 14 anni e che abbiamo il consenso dei
propri genitori. <br />
<br />
La Societ&agrave;, peraltro, incoraggia l&rsquo;iscrizione anche dei
genitori degli Utenti registrati minorenni: in questo modo i genitori
hanno la possibilit&agrave; di usufruire dei Servizi e di essere sempre
al corrente delle iniziative che la Societ&agrave; mette a disposizione
dei figli, e di verificarne cos&igrave; la rispondenza alle proprie
aspettative ed ai propri modelli e percorsi educativi. <br />
<br />
<strong>TITOLARE E RESPONSABILI DEL TRATTAMENTO</strong> <br />
<br />
11. Il Titolare del trattamento dei dati personali &egrave;
L&rsquo;Or&eacute;al Italia S.p.A., con sede legale in Via Garibaldi 42,
Torino. <br />
<br />
Il Responsabile del trattamento dei dati personali &egrave;: <br />
- DM Group S.p.A., con sede in Via Lessolo 19, Torino. <br />
<br />
<strong>COMUNICAZIONE DEI DATI</strong> <br />
<br />
12. In considerazione della natura globale delle attivit&agrave; della
Societ&agrave;, i dati potranno essere trasferiti ai computer server
situati in altri paesi nell&rsquo;ambito della Comunit&agrave; Europea.
Ogni trasferimento di dati avviene in conformit&agrave; con la legge
sulla tutela dei dati del Regno Unito e dell&rsquo;Unione Europea. <br />
<br />
<strong>MODIFICHE ALLA PRESENTE INFORMATIVA</strong> <br />
<br />
13. Fermo restando che la Societ&agrave; in ogni caso non procede ad
operazioni di trattamento diverse da quelle espressamente autorizzate
e/o richieste da ciascun Utente, la presente informativa pu&ograve;
essere fatta oggetto di modifiche per conformarsi a nuove disposizioni
di legge o alle mutate politiche di trattamento dei dati della
Societ&agrave;. <br />
<br />
Ogni versione aggiornata della presente informativa verr&agrave; resa
disponibile sul Sito nella sezione dedicata: la Societ&agrave; invita
pertanto tutti gli Utenti a consultare periodicamente il Sito per essere
sempre informati dell&rsquo;ultima versione caricata. <br />
<br />
<br />
</p>

</div>
</div>


<div id="contatti-box" style="display: none">
<div id="contatti-box-content" style="padding: 20px">

<p align="justify"><strong>Servizio Consumatori</strong><br />
<br />
Numero Verde:<br />
<strong>800-812073</strong><br />
(dal luned&igrave; al venerd&igrave;, dalle 9 alle 18)<br />
<br />
Indirizzo:<br />
Via Garibaldi 42- 10122 Torino</p>
</div>
</div>
<script type="text/javascript" src="jquery.min.js"></script> <script
	type="text/javascript"> 
<?php include "data.txt";?>
</script> <script type="text/javascript"> 
// returns array of elements whose 'prop' property is 'value'
function filterByProperty(arr, prop, value) {
    return $.grep(arr, function (item) { return item[prop] == value });
}

// populates select list from array of items given as objects: { name: 'text', value: 'value' }

                     function populateSelect(prov, el, items) {
                         el.options.length = 0;
                         if (items.length > 0)
                             if(prov==0){
                             	el.options[0] = new Option('Selezionare la Regione', '');
                             }else if(prov==1){
                              	el.options[0] = new Option('Selezionare la Provincia', '');
                             }else{
                              	el.options[0] = new Option('Selezionare il Comune', '');
                             }
                         $.each(items, function () {
                             el.options[el.options.length] = new Option(this.name, this.value);
                         });
                     }

// initialization
$(document).ready(function () {
    // populating 1st select list
    populateSelect(0,$('#region').get(0), $.map(localita, function(region) { return { name: region.name, value: region.name} }));

    // populating 2nd select list
    $('#region').bind('change', function() {
        var regionName = this.value;        
        var carMaker = filterByProperty(localita, "name", regionName),
                provinces = [];

//		carMaker = carMaker[0].replace("'","\'");
        if (carMaker.length > 0)
                provinces = $.map(carMaker[0].provinces, function(province) { return { name: province.name, value: regionName + '.' + province.name} });

        populateSelect(1,$('#province').get(0), provinces);
        $('#province').trigger('change');
    });

    // populating 3rd select list
    $('#province').bind('change', function () {

        var nameAndModel = this.value.split('.'),
                towns = [];

        if (2 == nameAndModel.length) {
                var regionName = nameAndModel[0], 
                        carModel = nameAndModel[1],
                        carMaker = filterByProperty(localita, 'name', regionName);

                if (carMaker.length > 0) {
                        var province = filterByProperty(carMaker[0].provinces, 'name', carModel)

                        if (province.length > 0)
                                towns = $.map(province[0].towns, function(town) { return { name: town, value: regionName + '.' + carModel + '.' + town} })
                }
        }

        populateSelect(2,$('#town').get(0), towns);
    })

    // alerting value on 3rd select list change
    $('#town').bind('change', function () { 
        if (this.value.length > 0)
        	$("#searchForm").submit();
    })
});

</script>
<script>
  // attach a submit handler to the form
  $("#searchForm").submit(function(event) {
    event.preventDefault(); 
        
    // get some values from elements on the page:
    var $form = $( this );
    var term = "";
    var prov = "";
    var tow = "";
    var url = $form.attr( 'action' );
        
    var tmp;

        if($('#region').val()!=null){
	        term = $('#region').val();
		}
        if($('#province').val()!=null){
			tmp = $('#province').val().split('.'),
    	    prov = tmp[1];
		}
        if($('#town').val()!=null){
            tmp = $('#town').val().split('.'),
        	tow =  tmp[2];
		}

// Send the data using post and put the results in a div

    $.post( url, { province: prov, region: term, town: tow } ,
      function( data ) {
//      	alert(data);
//          var content = $( data ).find( '#content' );
          $( "#result" ).html( data );
      }
    );
  });
</script>

<script type="text/javascript"> 
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-22773381-1']);
	_gaq.push(['_trackPageview']);
 
(function() {
	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
</script>


</body>
</html>
