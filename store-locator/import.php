<?php
include "config.inc.php";

$csvname = "Punti vendita FP-tabbed.txt";

if (file_exists($csvname)) {
	try
	{
		$db_connection = mysql_connect($DB_HOST, $DB_USER, $DB_PASS);
		mysql_select_db($DB_NAME);
		if (!$db_connection)
		{
			throw new Exception('Errore di connessione al db');
		}

		$query = 'DROP TABLE prov_punti_vendita ';
		mysql_query($query, $db_connection);
//		$query = 'TRUNCATE TABLE prov_punti_vendita ';
//		mysql_query($query, $db_connection);
		
		$query = 'CREATE TABLE prov_punti_vendita ' .
		'(nome TEXT, localita TEXT, via TEXT, cap TEXT, provincia TEXT, regione TEXT)ENGINE = MYISAM CHARACTER SET utf8 COLLATE utf8_general_ci;';
		mysql_query($query, $db_connection);
		
		$var=file_get_contents("Punti vendita FP-tabbed.txt");
		$var=explode("\r", $var);
		foreach ($var as $line=>$data) {
			$content = explode("\t", $data);
			$query = 'INSERT INTO prov_punti_vendita (nome, localita, via, cap, provincia, regione) VALUES (\''.mysql_escape_string($content[0]).'\', \''.mysql_escape_string($content[1]).'\', \''.mysql_escape_string($content[2]).'\', \''.mysql_escape_string($content[3]).'\', \''.mysql_escape_string($content[4]).'\', \''.mysql_escape_string($content[5]).'\')';
			mysql_query($query, $db_connection);
		}

	}
	catch(Exception $e)
	{
		echo return_message(false, $e->getMessage());
		die();
	}
}
include "locations.php";
include "store-locator.php";