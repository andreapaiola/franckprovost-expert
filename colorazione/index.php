<?php 
require('functions.php');
$xml = simplexml_load_file('catalogo.xml');
$resp = get_product($xml, get_product_id());
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">	
<link href="../css/style.css" rel="stylesheet" type="text/css">
<link href="../css/catalogo-tinte.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>
<!--popover-->
<script type="text/javascript" src="../js/popover/popover.js"></script>
</head>
<body>
<?php if (visualization_mode() == 1) : ?>
<script type="text/javascript">
$(window).load(function(){
	setTimeout(function() {$('#preloader').hide();},1000);
});
</script>
<div id="preloader"><div id="preloader-inner"><span>in caricamento</span></div></div> <!--preloader-->
<?php endif; ?>
	
	<div id="scheda-prodotto">
		<div class="hero-unit" style="background-image:url(../img/catalogo-tinte/pack-overlayer/<?php echo($resp[0]->fotopack); ?>)">	
			<h1><?php echo($resp[0]->nome); ?></h1>
			<?php if ($resp[0]->layout == 1) : ?>
			<p><?php echo(xml_to_html($resp[0]->descrizione) ); ?></p>
			<?php else : ?>
			<div class="layout-02">
				<p>Per la prima volta, direttamente dai saloni di <strong>Franck Provost</strong> a Parigi, il parrucchiere delle star arriva a casa con la sua 1&deg; colorazione professionale
studiata appositamente per valorizzare i riflessi<br/> 
e la brillantezza dalle radici alle punte.</p>
				<h2>APPLICAZIONE PROFESSIONALE CON SPUGNA</h2>
				<ul>
					<li>
						Permette una ripartizione facile e omogenea del<br/>
						colore su tutta la lunghezza dei capelli.
					</li>
					<li>
						Dona riflessi e brillantezza fino alle punte.						
					</li>
					<li>Stesura facile e omogenea.</li>
				</ul>
			</div> <!-- layout-spugna -->
			<?php endif; ?>	 	
		</div> <!--hero-unit-->
		<div class="risultati-tinta">
			<h2>Scopri il risultato<br> in base al colore di partenza</h2>
			<p>Il risultato colore dipende dal vostro colore di partenza. Come nei miei saloni la scelta di una colorazione permanente necessita di una grande attenzione.</p>
			<img src="../img/catalogo-tinte/risultato-colore/<?php echo($resp[0]->fotorisultati); ?>">
			<img src="../img/catalogo-tinte/scopri-i-miei-consigli.png" class="scopri-i-miei-consigli">
		</div> <!--risultati-tinta-->			
		<div class="composit">
			<ul>
				<li class="t1">
					<img src="../img/catalogo-tinte/composit/gamma_01.png">
					<a rel="shadowbox[composit];width=905;height=600" href="index.php?prod=1">
						<img src="../img/catalogo-tinte/pack-zoom/1.0.png" class="tooltip">
					</a>
				</li>
				<li class="t17">
                    <img src="../img/catalogo-tinte/composit/gamma_02.png">
                    <a rel="shadowbox[composit];width=905;height=600" href="index.php?prod=17">
                        <img src="../img/catalogo-tinte/pack-zoom/2.1.png" class="tooltip">
                    </a>
                </li>
				<li class="t2">
					<img src="../img/catalogo-tinte/composit/gamma_03.png">
					<a rel="shadowbox[composit];width=905;height=600"  href="index.php?prod=2">
						<img src="../img/catalogo-tinte/pack-zoom/3.0.png" class="tooltip">
					</a>							
				</li>
				<!--li class="t16">
                    <img src="../img/catalogo-tinte/composit/3.3.png">
                    <a rel="shadowbox[composit];width=905;height=600"  href="index.php?prod=16">
                        <img src="../img/catalogo-tinte/pack-zoom/3.3.png" class="tooltip">
                    </a>                            
                </li-->
				<li class="t3">
					<img src="../img/catalogo-tinte/composit/gamma_04.png">
					<a rel="shadowbox[composit];width=905;height=600"  href="index.php?prod=3">
						<img src="../img/catalogo-tinte/pack-zoom/4.0.png" class="tooltip">
					</a>
				</li>
				<li class="t4">
					<img src="../img/catalogo-tinte/composit/gamma_05.png">
					<a rel="shadowbox[composit];width=905;height=600"  href="index.php?prod=4">
						<img src="../img/catalogo-tinte/pack-zoom/4.15.png" class="tooltip">
					</a>
				</li>
				<li class="t5">
					<img src="../img/catalogo-tinte/composit/gamma_06.png">
					<a rel="shadowbox[composit];width=905;height=600"  href="index.php?prod=5">
						<img src="../img/catalogo-tinte/pack-zoom/4.26.png" class="tooltip">
					</a>
				</li>
				<li class="t6">
					<img src="../img/catalogo-tinte/composit/gamma_07.png">
					<a rel="shadowbox[composit];width=905;height=600"  href="index.php?prod=6">
						<img src="../img/catalogo-tinte/pack-zoom/4.3.png" class="tooltip">
					</a>
				</li>
				<li class="t7">
					<img src="../img/catalogo-tinte/composit/gamma_08.png">
					<a rel="shadowbox[composit];width=905;height=600"  href="index.php?prod=7">
						<img src="../img/catalogo-tinte/pack-zoom/4.5.png" class="tooltip">
					</a>
				</li>																					
				<li class="t8">
					<img src="../img/catalogo-tinte/composit/gamma_09.png">
					<a rel="shadowbox[composit];width=905;height=600"  href="index.php?prod=8">
						<img src="../img/catalogo-tinte/pack-zoom/5.0.png" class="tooltip">
					</a>
				</li>
				<li class="t9">
					<img src="../img/catalogo-tinte/composit/gamma_10.png">
					<a rel="shadowbox[composit];width=905;height=600"  href="index.php?prod=9">
						<img src="../img/catalogo-tinte/pack-zoom/5.3.png" class="tooltip">
					</a>
				</li>
				<li class="t19">
                    <img src="../img/catalogo-tinte/composit/gamma_11.png">
					<a rel="shadowbox[composit];width=905;height=600"  href="index.php?prod=10">
						<img src="../img/catalogo-tinte/pack-zoom/6.0.png" class="tooltip">
					</a>
                </li>
                <li class="t20">
                    <img src="../img/catalogo-tinte/composit/gamma_12.png">
					<a rel="shadowbox[composit];width=905;height=600"  href="index.php?prod=11">
						<img src="../img/catalogo-tinte/pack-zoom/6.66.png" class="tooltip">
					</a>
                </li>
				<li class="t10">
					<img src="../img/catalogo-tinte/composit/gamma_13.png">
                    <a rel="shadowbox[composit];width=905;height=600"  href="index.php?prod=18">
                        <img src="../img/catalogo-tinte/pack-zoom/7.0.png" class="tooltip">
                    </a>
				</li>
				<li class="t21">
                    <img src="../img/catalogo-tinte/composit/gamma_14.png">
					<a rel="shadowbox[composit];width=905;height=600"  href="index.php?prod=12">
						<img src="../img/catalogo-tinte/pack-zoom/7.3.png" class="tooltip">
					</a>
                </li>
                <li class="t22">
                    <img src="../img/catalogo-tinte/composit/gamma_15.png">
					<a rel="shadowbox[composit];width=905;height=600"  href="index.php?prod=13">
						<img src="../img/catalogo-tinte/pack-zoom/8.0.png" class="tooltip">
					</a>
                </li>
				<li class="t11">
					<img src="../img/catalogo-tinte/composit/gamma_16.png">
					<a rel="shadowbox[composit];width=905;height=600"  href="index.php?prod=14">
						<img src="../img/catalogo-tinte/pack-zoom/9.0.png" class="tooltip">
					</a>
				</li>
				<li class="t18">
                    <img src="../img/catalogo-tinte/composit/gamma_17.png">
					<a rel="shadowbox[composit];width=905;height=600"  href="index.php?prod=15">
						<img src="../img/catalogo-tinte/pack-zoom/10.23.png" class="tooltip">
					</a>
                </li>
				<li class="t12">
					<img src="../img/catalogo-tinte/composit/gamma_18.png">
                    <a rel="shadowbox[composit];width=905;height=600"  href="index.php?prod=19">
                        <img src="../img/catalogo-tinte/pack-zoom/5.35.png" class="tooltip">
                    </a>
				</li>
				<li class="t13">
					<img src="../img/catalogo-tinte/composit/gamma_19.png">
                    <a rel="shadowbox[composit];width=905;height=600"  href="index.php?prod=20">
                        <img src="../img/catalogo-tinte/pack-zoom/5.50.png" class="tooltip">
                    </a>
				</li>
				<!--li class="t14">
					<img src="../img/catalogo-tinte/composit/6.45.png">
                    <a rel="shadowbox[composit];width=905;height=600"  href="index.php?prod=21">
                        <img src="../img/catalogo-tinte/pack-zoom/6.45.png" class="tooltip">
                    </a>
				</li-->
				<!--li class="t15">
					<img src="../img/catalogo-tinte/composit/6.56.png">
                    <a rel="shadowbox[composit];width=905;height=600"  href="index.php?prod=22">
                        <img src="../img/catalogo-tinte/pack-zoom/6.56.png" class="tooltip">
                    </a>
				</li-->
			</ul>
		</div> <!--composit-->
	</div> <!--scheda-prodotto--> 

</body>
</html>
