<?php
function get_product_id() {
	if (isset($_GET['prod'])) {return $_GET['prod'];} else return 1;
} //gets product id

function visualization_mode() {
	if (isset($_GET['v'])) {return 1;} else return 0;
} //gets product id

function xml_to_html($stringa) {
	$ent = array(
		'<![CDATA[' => '', 
		']]>' => '', 
		'[b]' => '<strong>',
		'[/b]' => '</strong>' 
	);
	$result = htmlentities(utf8_decode($stringa));
	return strtr(stripslashes($result), $ent);
} //converts xml string to html entities

function get_product($xml_source, $product_id){
	$product_id=get_product_id();
	$response = $xml_source->xpath('//prodotto[id='.$product_id.']');
	if ( isset($response[0]->id) ) {
		return ($xml_source->xpath('//prodotto[id='.$product_id.']'));	
	}
		return ($xml_source->xpath('//prodotto[id=1]'));
} //returns a xml node with the product
?>