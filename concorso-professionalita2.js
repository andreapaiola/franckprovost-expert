
var form1Submit;

var initForm = function(){
    /*
     chosen https://github.com/harvesthq/chosen
     */
    (function() {
        var $, AbstractChosen, Chosen, SelectParser, _ref,
            __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
            __hasProp = {}.hasOwnProperty,
            __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

        SelectParser = (function() {
            function SelectParser() {
                this.options_index = 0;
                this.parsed = [];
            }

            SelectParser.prototype.add_node = function(child) {
                if (child.nodeName.toUpperCase() === "OPTGROUP") {
                    return this.add_group(child);
                } else {
                    return this.add_option(child);
                }
            };

            SelectParser.prototype.add_group = function(group) {
                var group_position, option, _i, _len, _ref, _results;
                group_position = this.parsed.length;
                this.parsed.push({
                    array_index: group_position,
                    group: true,
                    label: this.escapeExpression(group.label),
                    title: group.title ? group.title : void 0,
                    children: 0,
                    disabled: group.disabled,
                    classes: group.className
                });
                _ref = group.childNodes;
                _results = [];
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    option = _ref[_i];
                    _results.push(this.add_option(option, group_position, group.disabled));
                }
                return _results;
            };

            SelectParser.prototype.add_option = function(option, group_position, group_disabled) {
                if (option.nodeName.toUpperCase() === "OPTION") {
                    if (option.text !== "") {
                        if (group_position != null) {
                            this.parsed[group_position].children += 1;
                        }
                        this.parsed.push({
                            array_index: this.parsed.length,
                            options_index: this.options_index,
                            value: option.value,
                            text: option.text,
                            html: option.innerHTML,
                            title: option.title ? option.title : void 0,
                            selected: option.selected,
                            disabled: group_disabled === true ? group_disabled : option.disabled,
                            group_array_index: group_position,
                            group_label: group_position != null ? this.parsed[group_position].label : null,
                            classes: option.className,
                            style: option.style.cssText
                        });
                    } else {
                        this.parsed.push({
                            array_index: this.parsed.length,
                            options_index: this.options_index,
                            empty: true
                        });
                    }
                    return this.options_index += 1;
                }
            };

            SelectParser.prototype.escapeExpression = function(text) {
                var map, unsafe_chars;
                if ((text == null) || text === false) {
                    return "";
                }
                if (!/[\&\<\>\"\'\`]/.test(text)) {
                    return text;
                }
                map = {
                    "<": "&lt;",
                    ">": "&gt;",
                    '"': "&quot;",
                    "'": "&#x27;",
                    "`": "&#x60;"
                };
                unsafe_chars = /&(?!\w+;)|[\<\>\"\'\`]/g;
                return text.replace(unsafe_chars, function(chr) {
                    return map[chr] || "&amp;";
                });
            };

            return SelectParser;

        })();

        SelectParser.select_to_array = function(select) {
            var child, parser, _i, _len, _ref;
            parser = new SelectParser();
            _ref = select.childNodes;
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                child = _ref[_i];
                parser.add_node(child);
            }
            return parser.parsed;
        };

        AbstractChosen = (function() {
            function AbstractChosen(form_field, options) {
                this.form_field = form_field;
                this.options = options != null ? options : {};
                this.label_click_handler = __bind(this.label_click_handler, this);
                if (!AbstractChosen.browser_is_supported()) {
                    return;
                }
                this.is_multiple = this.form_field.multiple;
                this.set_default_text();
                this.set_default_values();
                this.setup();
                this.set_up_html();
                this.register_observers();
                this.on_ready();
            }

            AbstractChosen.prototype.set_default_values = function() {
                var _this = this;
                this.click_test_action = function(evt) {
                    return _this.test_active_click(evt);
                };
                this.activate_action = function(evt) {
                    return _this.activate_field(evt);
                };
                this.active_field = false;
                this.mouse_on_container = false;
                this.results_showing = false;
                this.result_highlighted = null;
                this.is_rtl = this.options.rtl || /\bchosen-rtl\b/.test(this.form_field.className);
                this.allow_single_deselect = (this.options.allow_single_deselect != null) && (this.form_field.options[0] != null) && this.form_field.options[0].text === "" ? this.options.allow_single_deselect : false;
                this.disable_search_threshold = this.options.disable_search_threshold || 0;
                this.disable_search = this.options.disable_search || false;
                this.enable_split_word_search = this.options.enable_split_word_search != null ? this.options.enable_split_word_search : true;
                this.group_search = this.options.group_search != null ? this.options.group_search : true;
                this.search_contains = this.options.search_contains || false;
                this.single_backstroke_delete = this.options.single_backstroke_delete != null ? this.options.single_backstroke_delete : true;
                this.max_selected_options = this.options.max_selected_options || Infinity;
                this.inherit_select_classes = this.options.inherit_select_classes || false;
                this.display_selected_options = this.options.display_selected_options != null ? this.options.display_selected_options : true;
                this.display_disabled_options = this.options.display_disabled_options != null ? this.options.display_disabled_options : true;
                this.include_group_label_in_selected = this.options.include_group_label_in_selected || false;
                this.max_shown_results = this.options.max_shown_results || Number.POSITIVE_INFINITY;
                this.case_sensitive_search = this.options.case_sensitive_search || false;
                return this.hide_results_on_select = this.options.hide_results_on_select != null ? this.options.hide_results_on_select : true;
            };

            AbstractChosen.prototype.set_default_text = function() {
                if (this.form_field.getAttribute("data-placeholder")) {
                    this.default_text = this.form_field.getAttribute("data-placeholder");
                } else if (this.is_multiple) {
                    this.default_text = this.options.placeholder_text_multiple || this.options.placeholder_text || AbstractChosen.default_multiple_text;
                } else {
                    this.default_text = this.options.placeholder_text_single || this.options.placeholder_text || AbstractChosen.default_single_text;
                }
                this.default_text = this.escape_html(this.default_text);
                return this.results_none_found = this.form_field.getAttribute("data-no_results_text") || this.options.no_results_text || AbstractChosen.default_no_result_text;
            };

            AbstractChosen.prototype.choice_label = function(item) {
                if (this.include_group_label_in_selected && (item.group_label != null)) {
                    return "<b class='group-name'>" + item.group_label + "</b>" + item.html;
                } else {
                    return item.html;
                }
            };

            AbstractChosen.prototype.mouse_enter = function() {
                return this.mouse_on_container = true;
            };

            AbstractChosen.prototype.mouse_leave = function() {
                return this.mouse_on_container = false;
            };

            AbstractChosen.prototype.input_focus = function(evt) {
                var _this = this;
                if (this.is_multiple) {
                    if (!this.active_field) {
                        return setTimeout((function() {
                            return _this.container_mousedown();
                        }), 50);
                    }
                } else {
                    if (!this.active_field) {
                        return this.activate_field();
                    }
                }
            };

            AbstractChosen.prototype.input_blur = function(evt) {
                var _this = this;
                if (!this.mouse_on_container) {
                    this.active_field = false;
                    return setTimeout((function() {
                        return _this.blur_test();
                    }), 100);
                }
            };

            AbstractChosen.prototype.label_click_handler = function(evt) {
                if (this.is_multiple) {
                    return this.container_mousedown(evt);
                } else {
                    return this.activate_field();
                }
            };

            AbstractChosen.prototype.results_option_build = function(options) {
                var content, data, data_content, shown_results, _i, _len, _ref;
                content = '';
                shown_results = 0;
                _ref = this.results_data;
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    data = _ref[_i];
                    data_content = '';
                    if (data.group) {
                        data_content = this.result_add_group(data);
                    } else {
                        data_content = this.result_add_option(data);
                    }
                    if (data_content !== '') {
                        shown_results++;
                        content += data_content;
                    }
                    if (options != null ? options.first : void 0) {
                        if (data.selected && this.is_multiple) {
                            this.choice_build(data);
                        } else if (data.selected && !this.is_multiple) {
                            this.single_set_selected_text(this.choice_label(data));
                        }
                    }
                    if (shown_results >= this.max_shown_results) {
                        break;
                    }
                }
                return content;
            };

            AbstractChosen.prototype.result_add_option = function(option) {
                var classes, option_el;
                if (!option.search_match) {
                    return '';
                }
                if (!this.include_option_in_results(option)) {
                    return '';
                }
                classes = [];
                if (!option.disabled && !(option.selected && this.is_multiple)) {
                    classes.push("active-result");
                }
                if (option.disabled && !(option.selected && this.is_multiple)) {
                    classes.push("disabled-result");
                }
                if (option.selected) {
                    classes.push("result-selected");
                }
                if (option.group_array_index != null) {
                    classes.push("group-option");
                }
                if (option.classes !== "") {
                    classes.push(option.classes);
                }
                option_el = document.createElement("li");
                option_el.className = classes.join(" ");
                option_el.style.cssText = option.style;
                option_el.setAttribute("data-option-array-index", option.array_index);
                option_el.innerHTML = option.search_text;
                if (option.title) {
                    option_el.title = option.title;
                }
                return this.outerHTML(option_el);
            };

            AbstractChosen.prototype.result_add_group = function(group) {
                var classes, group_el;
                if (!(group.search_match || group.group_match)) {
                    return '';
                }
                if (!(group.active_options > 0)) {
                    return '';
                }
                classes = [];
                classes.push("group-result");
                if (group.classes) {
                    classes.push(group.classes);
                }
                group_el = document.createElement("li");
                group_el.className = classes.join(" ");
                group_el.innerHTML = group.search_text;
                if (group.title) {
                    group_el.title = group.title;
                }
                return this.outerHTML(group_el);
            };

            AbstractChosen.prototype.results_update_field = function() {
                this.set_default_text();
                if (!this.is_multiple) {
                    this.results_reset_cleanup();
                }
                this.result_clear_highlight();
                this.results_build();
                if (this.results_showing) {
                    return this.winnow_results();
                }
            };

            AbstractChosen.prototype.reset_single_select_options = function() {
                var result, _i, _len, _ref, _results;
                _ref = this.results_data;
                _results = [];
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    result = _ref[_i];
                    if (result.selected) {
                        _results.push(result.selected = false);
                    } else {
                        _results.push(void 0);
                    }
                }
                return _results;
            };

            AbstractChosen.prototype.results_toggle = function() {
                if (this.results_showing) {
                    return this.results_hide();
                } else {
                    return this.results_show();
                }
            };

            AbstractChosen.prototype.results_search = function(evt) {
                if (this.results_showing) {
                    return this.winnow_results();
                } else {
                    return this.results_show();
                }
            };

            AbstractChosen.prototype.winnow_results = function() {
                var escapedSearchText, highlightRegex, option, regex, results, results_group, searchText, startpos, text, _i, _len, _ref;
                this.no_results_clear();
                results = 0;
                searchText = this.get_search_text();
                escapedSearchText = searchText.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
                regex = this.get_search_regex(escapedSearchText);
                highlightRegex = this.get_highlight_regex(escapedSearchText);
                _ref = this.results_data;
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    option = _ref[_i];
                    option.search_match = false;
                    results_group = null;
                    if (this.include_option_in_results(option)) {
                        if (option.group) {
                            option.group_match = false;
                            option.active_options = 0;
                        }
                        if ((option.group_array_index != null) && this.results_data[option.group_array_index]) {
                            results_group = this.results_data[option.group_array_index];
                            if (results_group.active_options === 0 && results_group.search_match) {
                                results += 1;
                            }
                            results_group.active_options += 1;
                        }
                        option.search_text = option.group ? option.label : option.html;
                        if (!(option.group && !this.group_search)) {
                            option.search_match = this.search_string_match(option.search_text, regex);
                            if (option.search_match && !option.group) {
                                results += 1;
                            }
                            if (option.search_match) {
                                if (searchText.length) {
                                    startpos = option.search_text.search(highlightRegex);
                                    text = option.search_text.substr(0, startpos + searchText.length) + '</em>' + option.search_text.substr(startpos + searchText.length);
                                    option.search_text = text.substr(0, startpos) + '<em>' + text.substr(startpos);
                                }
                                if (results_group != null) {
                                    results_group.group_match = true;
                                }
                            } else if ((option.group_array_index != null) && this.results_data[option.group_array_index].search_match) {
                                option.search_match = true;
                            }
                        }
                    }
                }
                this.result_clear_highlight();
                if (results < 1 && searchText.length) {
                    this.update_results_content("");
                    return this.no_results(searchText);
                } else {
                    this.update_results_content(this.results_option_build());
                    return this.winnow_results_set_highlight();
                }
            };

            AbstractChosen.prototype.get_search_regex = function(escaped_search_string) {
                var regex_anchor, regex_flag;
                regex_anchor = this.search_contains ? "" : "^";
                regex_flag = this.case_sensitive_search ? "" : "i";
                return new RegExp(regex_anchor + escaped_search_string, regex_flag);
            };

            AbstractChosen.prototype.get_highlight_regex = function(escaped_search_string) {
                var regex_anchor, regex_flag;
                regex_anchor = this.search_contains ? "" : "\\b";
                regex_flag = this.case_sensitive_search ? "" : "i";
                return new RegExp(regex_anchor + escaped_search_string, regex_flag);
            };

            AbstractChosen.prototype.search_string_match = function(search_string, regex) {
                var part, parts, _i, _len;
                if (regex.test(search_string)) {
                    return true;
                } else if (this.enable_split_word_search && (search_string.indexOf(" ") >= 0 || search_string.indexOf("[") === 0)) {
                    parts = search_string.replace(/\[|\]/g, "").split(" ");
                    if (parts.length) {
                        for (_i = 0, _len = parts.length; _i < _len; _i++) {
                            part = parts[_i];
                            if (regex.test(part)) {
                                return true;
                            }
                        }
                    }
                }
            };

            AbstractChosen.prototype.choices_count = function() {
                var option, _i, _len, _ref;
                if (this.selected_option_count != null) {
                    return this.selected_option_count;
                }
                this.selected_option_count = 0;
                _ref = this.form_field.options;
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    option = _ref[_i];
                    if (option.selected) {
                        this.selected_option_count += 1;
                    }
                }
                return this.selected_option_count;
            };

            AbstractChosen.prototype.choices_click = function(evt) {
                evt.preventDefault();
                this.activate_field();
                if (!(this.results_showing || this.is_disabled)) {
                    return this.results_show();
                }
            };

            AbstractChosen.prototype.keydown_checker = function(evt) {
                var stroke, _ref;
                stroke = (_ref = evt.which) != null ? _ref : evt.keyCode;
                this.search_field_scale();
                if (stroke !== 8 && this.pending_backstroke) {
                    this.clear_backstroke();
                }
                switch (stroke) {
                    case 8:
                        this.backstroke_length = this.get_search_field_value().length;
                        break;
                    case 9:
                        if (this.results_showing && !this.is_multiple) {
                            this.result_select(evt);
                        }
                        this.mouse_on_container = false;
                        break;
                    case 13:
                        if (this.results_showing) {
                            evt.preventDefault();
                        }
                        break;
                    case 27:
                        if (this.results_showing) {
                            evt.preventDefault();
                        }
                        break;
                    case 32:
                        if (this.disable_search) {
                            evt.preventDefault();
                        }
                        break;
                    case 38:
                        evt.preventDefault();
                        this.keyup_arrow();
                        break;
                    case 40:
                        evt.preventDefault();
                        this.keydown_arrow();
                        break;
                }
            };

            AbstractChosen.prototype.keyup_checker = function(evt) {
                var stroke, _ref;
                stroke = (_ref = evt.which) != null ? _ref : evt.keyCode;
                this.search_field_scale();
                switch (stroke) {
                    case 8:
                        if (this.is_multiple && this.backstroke_length < 1 && this.choices_count() > 0) {
                            this.keydown_backstroke();
                        } else if (!this.pending_backstroke) {
                            this.result_clear_highlight();
                            this.results_search();
                        }
                        break;
                    case 13:
                        evt.preventDefault();
                        if (this.results_showing) {
                            this.result_select(evt);
                        }
                        break;
                    case 27:
                        if (this.results_showing) {
                            this.results_hide();
                        }
                        break;
                    case 9:
                    case 16:
                    case 17:
                    case 18:
                    case 38:
                    case 40:
                    case 91:
                        break;
                    default:
                        this.results_search();
                        break;
                }
            };

            AbstractChosen.prototype.clipboard_event_checker = function(evt) {
                var _this = this;
                if (this.is_disabled) {
                    return;
                }
                return setTimeout((function() {
                    return _this.results_search();
                }), 50);
            };

            AbstractChosen.prototype.container_width = function() {
                if (this.options.width != null) {
                    return this.options.width;
                } else {
                    return "" + this.form_field.offsetWidth + "px";
                }
            };

            AbstractChosen.prototype.include_option_in_results = function(option) {
                if (this.is_multiple && (!this.display_selected_options && option.selected)) {
                    return false;
                }
                if (!this.display_disabled_options && option.disabled) {
                    return false;
                }
                if (option.empty) {
                    return false;
                }
                return true;
            };

            AbstractChosen.prototype.search_results_touchstart = function(evt) {
                this.touch_started = true;
                return this.search_results_mouseover(evt);
            };

            AbstractChosen.prototype.search_results_touchmove = function(evt) {
                this.touch_started = false;
                return this.search_results_mouseout(evt);
            };

            AbstractChosen.prototype.search_results_touchend = function(evt) {
                if (this.touch_started) {
                    return this.search_results_mouseup(evt);
                }
            };

            AbstractChosen.prototype.outerHTML = function(element) {
                var tmp;
                if (element.outerHTML) {
                    return element.outerHTML;
                }
                tmp = document.createElement("div");
                tmp.appendChild(element);
                return tmp.innerHTML;
            };

            AbstractChosen.prototype.get_single_html = function() {
                return "<a class=\"chosen-single chosen-default\">\n  <span>" + this.default_text + "</span>\n  <div><b></b></div>\n</a>\n<div class=\"chosen-drop\">\n  <div class=\"chosen-search\">\n    <input class=\"chosen-search-input\" type=\"text\" autocomplete=\"off\" />\n  </div>\n  <ul class=\"chosen-results\"></ul>\n</div>";
            };

            AbstractChosen.prototype.get_multi_html = function() {
                return "<ul class=\"chosen-choices\">\n  <li class=\"search-field\">\n    <input class=\"chosen-search-input\" type=\"text\" autocomplete=\"off\" value=\"" + this.default_text + "\" />\n  </li>\n</ul>\n<div class=\"chosen-drop\">\n  <ul class=\"chosen-results\"></ul>\n</div>";
            };

            AbstractChosen.prototype.get_no_results_html = function(terms) {
                return "<li class=\"no-results\">\n  " + this.results_none_found + " <span>" + terms + "</span>\n</li>";
            };

            AbstractChosen.browser_is_supported = function() {
                if ("Microsoft Internet Explorer" === window.navigator.appName) {
                    return document.documentMode >= 8;
                }
                if (/iP(od|hone)/i.test(window.navigator.userAgent) || /IEMobile/i.test(window.navigator.userAgent) || /Windows Phone/i.test(window.navigator.userAgent) || /BlackBerry/i.test(window.navigator.userAgent) || /BB10/i.test(window.navigator.userAgent) || /Android.*Mobile/i.test(window.navigator.userAgent)) {
                    return false;
                }
                return true;
            };

            AbstractChosen.default_multiple_text = "Select Some Options";

            AbstractChosen.default_single_text = "Select an Option";

            AbstractChosen.default_no_result_text = "No results match";

            return AbstractChosen;

        })();

        $ = jQuery;

        $.fn.extend({
            chosen: function(options) {
                if (!AbstractChosen.browser_is_supported()) {
                    return this;
                }
                return this.each(function(input_field) {
                    var $this, chosen;
                    $this = $(this);
                    chosen = $this.data('chosen');
                    if (options === 'destroy') {
                        if (chosen instanceof Chosen) {
                            chosen.destroy();
                        }
                        return;
                    }
                    if (!(chosen instanceof Chosen)) {
                        $this.data('chosen', new Chosen(this, options));
                    }
                });
            }
        });

        Chosen = (function(_super) {
            __extends(Chosen, _super);

            function Chosen() {
                _ref = Chosen.__super__.constructor.apply(this, arguments);
                return _ref;
            }

            Chosen.prototype.setup = function() {
                this.form_field_jq = $(this.form_field);
                return this.current_selectedIndex = this.form_field.selectedIndex;
            };

            Chosen.prototype.set_up_html = function() {
                var container_classes, container_props;
                container_classes = ["chosen-container"];
                container_classes.push("chosen-container-" + (this.is_multiple ? "multi" : "single"));
                if (this.inherit_select_classes && this.form_field.className) {
                    container_classes.push(this.form_field.className);
                }
                if (this.is_rtl) {
                    container_classes.push("chosen-rtl");
                }
                container_props = {
                    'class': container_classes.join(' '),
                    'title': this.form_field.title
                };
                if (this.form_field.id.length) {
                    container_props.id = this.form_field.id.replace(/[^\w]/g, '_') + "_chosen";
                }
                this.container = $("<div />", container_props);
                this.container.width(this.container_width());
                if (this.is_multiple) {
                    this.container.html(this.get_multi_html());
                } else {
                    this.container.html(this.get_single_html());
                }
                this.form_field_jq.hide().after(this.container);
                this.dropdown = this.container.find('div.chosen-drop').first();
                this.search_field = this.container.find('input').first();
                this.search_results = this.container.find('ul.chosen-results').first();
                this.search_field_scale();
                this.search_no_results = this.container.find('li.no-results').first();
                if (this.is_multiple) {
                    this.search_choices = this.container.find('ul.chosen-choices').first();
                    this.search_container = this.container.find('li.search-field').first();
                } else {
                    this.search_container = this.container.find('div.chosen-search').first();
                    this.selected_item = this.container.find('.chosen-single').first();
                }
                this.results_build();
                this.set_tab_index();
                return this.set_label_behavior();
            };

            Chosen.prototype.on_ready = function() {
                return this.form_field_jq.trigger("chosen:ready", {
                    chosen: this
                });
            };

            Chosen.prototype.register_observers = function() {
                var _this = this;
                this.container.bind('touchstart.chosen', function(evt) {
                    _this.container_mousedown(evt);
                });
                this.container.bind('touchend.chosen', function(evt) {
                    _this.container_mouseup(evt);
                });
                this.container.bind('mousedown.chosen', function(evt) {
                    _this.container_mousedown(evt);
                });
                this.container.bind('mouseup.chosen', function(evt) {
                    _this.container_mouseup(evt);
                });
                this.container.bind('mouseenter.chosen', function(evt) {
                    _this.mouse_enter(evt);
                });
                this.container.bind('mouseleave.chosen', function(evt) {
                    _this.mouse_leave(evt);
                });
                this.search_results.bind('mouseup.chosen', function(evt) {
                    _this.search_results_mouseup(evt);
                });
                this.search_results.bind('mouseover.chosen', function(evt) {
                    _this.search_results_mouseover(evt);
                });
                this.search_results.bind('mouseout.chosen', function(evt) {
                    _this.search_results_mouseout(evt);
                });
                this.search_results.bind('mousewheel.chosen DOMMouseScroll.chosen', function(evt) {
                    _this.search_results_mousewheel(evt);
                });
                this.search_results.bind('touchstart.chosen', function(evt) {
                    _this.search_results_touchstart(evt);
                });
                this.search_results.bind('touchmove.chosen', function(evt) {
                    _this.search_results_touchmove(evt);
                });
                this.search_results.bind('touchend.chosen', function(evt) {
                    _this.search_results_touchend(evt);
                });
                this.form_field_jq.bind("chosen:updated.chosen", function(evt) {
                    _this.results_update_field(evt);
                });
                this.form_field_jq.bind("chosen:activate.chosen", function(evt) {
                    _this.activate_field(evt);
                });
                this.form_field_jq.bind("chosen:open.chosen", function(evt) {
                    _this.container_mousedown(evt);
                });
                this.form_field_jq.bind("chosen:close.chosen", function(evt) {
                    _this.close_field(evt);
                });
                this.search_field.bind('blur.chosen', function(evt) {
                    _this.input_blur(evt);
                });
                this.search_field.bind('keyup.chosen', function(evt) {
                    _this.keyup_checker(evt);
                });
                this.search_field.bind('keydown.chosen', function(evt) {
                    _this.keydown_checker(evt);
                });
                this.search_field.bind('focus.chosen', function(evt) {
                    _this.input_focus(evt);
                });
                this.search_field.bind('cut.chosen', function(evt) {
                    _this.clipboard_event_checker(evt);
                });
                this.search_field.bind('paste.chosen', function(evt) {
                    _this.clipboard_event_checker(evt);
                });
                if (this.is_multiple) {
                    return this.search_choices.bind('click.chosen', function(evt) {
                        _this.choices_click(evt);
                    });
                } else {
                    return this.container.bind('click.chosen', function(evt) {
                        evt.preventDefault();
                    });
                }
            };

            Chosen.prototype.destroy = function() {
                $(this.container[0].ownerDocument).unbind('click.chosen', this.click_test_action);
                if (this.form_field_label.length > 0) {
                    this.form_field_label.unbind('click.chosen');
                }
                if (this.search_field[0].tabIndex) {
                    this.form_field_jq[0].tabIndex = this.search_field[0].tabIndex;
                }
                this.container.remove();
                this.form_field_jq.removeData('chosen');
                return this.form_field_jq.show();
            };

            Chosen.prototype.search_field_disabled = function() {
                this.is_disabled = this.form_field.disabled || this.form_field_jq.parents('fieldset').is(':disabled');
                this.container.toggleClass('chosen-disabled', this.is_disabled);
                this.search_field[0].disabled = this.is_disabled;
                if (!this.is_multiple) {
                    this.selected_item.unbind('focus.chosen', this.activate_field);
                }
                if (this.is_disabled) {
                    return this.close_field();
                } else if (!this.is_multiple) {
                    return this.selected_item.bind('focus.chosen', this.activate_field);
                }
            };

            Chosen.prototype.container_mousedown = function(evt) {
                var _ref1;
                if (this.is_disabled) {
                    return;
                }
                if (evt && ((_ref1 = evt.type) === 'mousedown' || _ref1 === 'touchstart') && !this.results_showing) {
                    evt.preventDefault();
                }
                if (!((evt != null) && ($(evt.target)).hasClass("search-choice-close"))) {
                    if (!this.active_field) {
                        if (this.is_multiple) {
                            this.search_field.val("");
                        }
                        $(this.container[0].ownerDocument).bind('click.chosen', this.click_test_action);
                        this.results_show();
                    } else if (!this.is_multiple && evt && (($(evt.target)[0] === this.selected_item[0]) || $(evt.target).parents("a.chosen-single").length)) {
                        evt.preventDefault();
                        this.results_toggle();
                    }
                    return this.activate_field();
                }
            };

            Chosen.prototype.container_mouseup = function(evt) {
                if (evt.target.nodeName === "ABBR" && !this.is_disabled) {
                    return this.results_reset(evt);
                }
            };

            Chosen.prototype.search_results_mousewheel = function(evt) {
                var delta;
                if (evt.originalEvent) {
                    delta = evt.originalEvent.deltaY || -evt.originalEvent.wheelDelta || evt.originalEvent.detail;
                }
                if (delta != null) {
                    evt.preventDefault();
                    if (evt.type === 'DOMMouseScroll') {
                        delta = delta * 40;
                    }
                    return this.search_results.scrollTop(delta + this.search_results.scrollTop());
                }
            };

            Chosen.prototype.blur_test = function(evt) {
                if (!this.active_field && this.container.hasClass("chosen-container-active")) {
                    return this.close_field();
                }
            };

            Chosen.prototype.close_field = function() {
                $(this.container[0].ownerDocument).unbind("click.chosen", this.click_test_action);
                this.active_field = false;
                this.results_hide();
                this.container.removeClass("chosen-container-active");
                this.clear_backstroke();
                this.show_search_field_default();
                this.search_field_scale();
                return this.search_field.blur();
            };

            Chosen.prototype.activate_field = function() {
                if (this.is_disabled) {
                    return;
                }
                this.container.addClass("chosen-container-active");
                this.active_field = true;
                this.search_field.val(this.search_field.val());
                return this.search_field.focus();
            };

            Chosen.prototype.test_active_click = function(evt) {
                var active_container;
                active_container = $(evt.target).closest('.chosen-container');
                if (active_container.length && this.container[0] === active_container[0]) {
                    return this.active_field = true;
                } else {
                    return this.close_field();
                }
            };

            Chosen.prototype.results_build = function() {
                this.parsing = true;
                this.selected_option_count = null;
                this.results_data = SelectParser.select_to_array(this.form_field);
                if (this.is_multiple) {
                    this.search_choices.find("li.search-choice").remove();
                } else if (!this.is_multiple) {
                    this.single_set_selected_text();
                    if (this.disable_search || this.form_field.options.length <= this.disable_search_threshold) {
                        this.search_field[0].readOnly = true;
                        this.container.addClass("chosen-container-single-nosearch");
                    } else {
                        this.search_field[0].readOnly = false;
                        this.container.removeClass("chosen-container-single-nosearch");
                    }
                }
                this.update_results_content(this.results_option_build({
                    first: true
                }));
                this.search_field_disabled();
                this.show_search_field_default();
                this.search_field_scale();
                return this.parsing = false;
            };

            Chosen.prototype.result_do_highlight = function(el) {
                var high_bottom, high_top, maxHeight, visible_bottom, visible_top;
                if (el.length) {
                    this.result_clear_highlight();
                    this.result_highlight = el;
                    this.result_highlight.addClass("highlighted");
                    maxHeight = parseInt(this.search_results.css("maxHeight"), 10);
                    visible_top = this.search_results.scrollTop();
                    visible_bottom = maxHeight + visible_top;
                    high_top = this.result_highlight.position().top + this.search_results.scrollTop();
                    high_bottom = high_top + this.result_highlight.outerHeight();
                    if (high_bottom >= visible_bottom) {
                        return this.search_results.scrollTop((high_bottom - maxHeight) > 0 ? high_bottom - maxHeight : 0);
                    } else if (high_top < visible_top) {
                        return this.search_results.scrollTop(high_top);
                    }
                }
            };

            Chosen.prototype.result_clear_highlight = function() {
                if (this.result_highlight) {
                    this.result_highlight.removeClass("highlighted");
                }
                return this.result_highlight = null;
            };

            Chosen.prototype.results_show = function() {
                if (this.is_multiple && this.max_selected_options <= this.choices_count()) {
                    this.form_field_jq.trigger("chosen:maxselected", {
                        chosen: this
                    });
                    return false;
                }
                this.container.addClass("chosen-with-drop");
                this.results_showing = true;
                this.search_field.focus();
                this.search_field.val(this.get_search_field_value());
                this.winnow_results();
                return this.form_field_jq.trigger("chosen:showing_dropdown", {
                    chosen: this
                });
            };

            Chosen.prototype.update_results_content = function(content) {
                return this.search_results.html(content);
            };

            Chosen.prototype.results_hide = function() {
                if (this.results_showing) {
                    this.result_clear_highlight();
                    this.container.removeClass("chosen-with-drop");
                    this.form_field_jq.trigger("chosen:hiding_dropdown", {
                        chosen: this
                    });
                }
                return this.results_showing = false;
            };

            Chosen.prototype.set_tab_index = function(el) {
                var ti;
                if (this.form_field.tabIndex) {
                    ti = this.form_field.tabIndex;
                    this.form_field.tabIndex = -1;
                    return this.search_field[0].tabIndex = ti;
                }
            };

            Chosen.prototype.set_label_behavior = function() {
                this.form_field_label = this.form_field_jq.parents("label");
                if (!this.form_field_label.length && this.form_field.id.length) {
                    this.form_field_label = $("label[for='" + this.form_field.id + "']");
                }
                if (this.form_field_label.length > 0) {
                    return this.form_field_label.bind('click.chosen', this.label_click_handler);
                }
            };

            Chosen.prototype.show_search_field_default = function() {
                if (this.is_multiple && this.choices_count() < 1 && !this.active_field) {
                    this.search_field.val(this.default_text);
                    return this.search_field.addClass("default");
                } else {
                    this.search_field.val("");
                    return this.search_field.removeClass("default");
                }
            };

            Chosen.prototype.search_results_mouseup = function(evt) {
                var target;
                target = $(evt.target).hasClass("active-result") ? $(evt.target) : $(evt.target).parents(".active-result").first();
                if (target.length) {
                    this.result_highlight = target;
                    this.result_select(evt);
                    return this.search_field.focus();
                }
            };

            Chosen.prototype.search_results_mouseover = function(evt) {
                var target;
                target = $(evt.target).hasClass("active-result") ? $(evt.target) : $(evt.target).parents(".active-result").first();
                if (target) {
                    return this.result_do_highlight(target);
                }
            };

            Chosen.prototype.search_results_mouseout = function(evt) {
                if ($(evt.target).hasClass("active-result" || $(evt.target).parents('.active-result').first())) {
                    return this.result_clear_highlight();
                }
            };

            Chosen.prototype.choice_build = function(item) {
                var choice, close_link,
                    _this = this;
                choice = $('<li />', {
                    "class": "search-choice"
                }).html("<span>" + (this.choice_label(item)) + "</span>");
                if (item.disabled) {
                    choice.addClass('search-choice-disabled');
                } else {
                    close_link = $('<a />', {
                        "class": 'search-choice-close',
                        'data-option-array-index': item.array_index
                    });
                    close_link.bind('click.chosen', function(evt) {
                        return _this.choice_destroy_link_click(evt);
                    });
                    choice.append(close_link);
                }
                return this.search_container.before(choice);
            };

            Chosen.prototype.choice_destroy_link_click = function(evt) {
                evt.preventDefault();
                evt.stopPropagation();
                if (!this.is_disabled) {
                    return this.choice_destroy($(evt.target));
                }
            };

            Chosen.prototype.choice_destroy = function(link) {
                if (this.result_deselect(link[0].getAttribute("data-option-array-index"))) {
                    if (this.active_field) {
                        this.search_field.focus();
                    } else {
                        this.show_search_field_default();
                    }
                    if (this.is_multiple && this.choices_count() > 0 && this.get_search_field_value().length < 1) {
                        this.results_hide();
                    }
                    link.parents('li').first().remove();
                    return this.search_field_scale();
                }
            };

            Chosen.prototype.results_reset = function() {
                this.reset_single_select_options();
                this.form_field.options[0].selected = true;
                this.single_set_selected_text();
                this.show_search_field_default();
                this.results_reset_cleanup();
                this.trigger_form_field_change();
                if (this.active_field) {
                    return this.results_hide();
                }
            };

            Chosen.prototype.results_reset_cleanup = function() {
                this.current_selectedIndex = this.form_field.selectedIndex;
                return this.selected_item.find("abbr").remove();
            };

            Chosen.prototype.result_select = function(evt) {
                var high, item;
                if (this.result_highlight) {
                    high = this.result_highlight;
                    this.result_clear_highlight();
                    if (this.is_multiple && this.max_selected_options <= this.choices_count()) {
                        this.form_field_jq.trigger("chosen:maxselected", {
                            chosen: this
                        });
                        return false;
                    }
                    if (this.is_multiple) {
                        high.removeClass("active-result");
                    } else {
                        this.reset_single_select_options();
                    }
                    high.addClass("result-selected");
                    item = this.results_data[high[0].getAttribute("data-option-array-index")];
                    item.selected = true;
                    this.form_field.options[item.options_index].selected = true;
                    this.selected_option_count = null;
                    if (this.is_multiple) {
                        this.choice_build(item);
                    } else {
                        this.single_set_selected_text(this.choice_label(item));
                    }
                    if (!(this.is_multiple && (!this.hide_results_on_select || (evt.metaKey || evt.ctrlKey)))) {
                        this.results_hide();
                        this.show_search_field_default();
                    }
                    if (this.is_multiple || this.form_field.selectedIndex !== this.current_selectedIndex) {
                        this.trigger_form_field_change({
                            selected: this.form_field.options[item.options_index].value
                        });
                    }
                    this.current_selectedIndex = this.form_field.selectedIndex;
                    evt.preventDefault();
                    return this.search_field_scale();
                }
            };

            Chosen.prototype.single_set_selected_text = function(text) {
                if (text == null) {
                    text = this.default_text;
                }
                if (text === this.default_text) {
                    this.selected_item.addClass("chosen-default");
                } else {
                    this.single_deselect_control_build();
                    this.selected_item.removeClass("chosen-default");
                }
                return this.selected_item.find("span").html(text);
            };

            Chosen.prototype.result_deselect = function(pos) {
                var result_data;
                result_data = this.results_data[pos];
                if (!this.form_field.options[result_data.options_index].disabled) {
                    result_data.selected = false;
                    this.form_field.options[result_data.options_index].selected = false;
                    this.selected_option_count = null;
                    this.result_clear_highlight();
                    if (this.results_showing) {
                        this.winnow_results();
                    }
                    this.trigger_form_field_change({
                        deselected: this.form_field.options[result_data.options_index].value
                    });
                    this.search_field_scale();
                    return true;
                } else {
                    return false;
                }
            };

            Chosen.prototype.single_deselect_control_build = function() {
                if (!this.allow_single_deselect) {
                    return;
                }
                if (!this.selected_item.find("abbr").length) {
                    this.selected_item.find("span").first().after("<abbr class=\"search-choice-close\"></abbr>");
                }
                return this.selected_item.addClass("chosen-single-with-deselect");
            };

            Chosen.prototype.get_search_field_value = function() {
                return this.search_field.val();
            };

            Chosen.prototype.get_search_text = function() {
                return this.escape_html($.trim(this.get_search_field_value()));
            };

            Chosen.prototype.escape_html = function(text) {
                return $('<div/>').text(text).html();
            };

            Chosen.prototype.winnow_results_set_highlight = function() {
                var do_high, selected_results;
                selected_results = !this.is_multiple ? this.search_results.find(".result-selected.active-result") : [];
                do_high = selected_results.length ? selected_results.first() : this.search_results.find(".active-result").first();
                if (do_high != null) {
                    return this.result_do_highlight(do_high);
                }
            };

            Chosen.prototype.no_results = function(terms) {
                var no_results_html;
                no_results_html = this.get_no_results_html(terms);
                this.search_results.append(no_results_html);
                return this.form_field_jq.trigger("chosen:no_results", {
                    chosen: this
                });
            };

            Chosen.prototype.no_results_clear = function() {
                return this.search_results.find(".no-results").remove();
            };

            Chosen.prototype.keydown_arrow = function() {
                var next_sib;
                if (this.results_showing && this.result_highlight) {
                    next_sib = this.result_highlight.nextAll("li.active-result").first();
                    if (next_sib) {
                        return this.result_do_highlight(next_sib);
                    }
                } else {
                    return this.results_show();
                }
            };

            Chosen.prototype.keyup_arrow = function() {
                var prev_sibs;
                if (!this.results_showing && !this.is_multiple) {
                    return this.results_show();
                } else if (this.result_highlight) {
                    prev_sibs = this.result_highlight.prevAll("li.active-result");
                    if (prev_sibs.length) {
                        return this.result_do_highlight(prev_sibs.first());
                    } else {
                        if (this.choices_count() > 0) {
                            this.results_hide();
                        }
                        return this.result_clear_highlight();
                    }
                }
            };

            Chosen.prototype.keydown_backstroke = function() {
                var next_available_destroy;
                if (this.pending_backstroke) {
                    this.choice_destroy(this.pending_backstroke.find("a").first());
                    return this.clear_backstroke();
                } else {
                    next_available_destroy = this.search_container.siblings("li.search-choice").last();
                    if (next_available_destroy.length && !next_available_destroy.hasClass("search-choice-disabled")) {
                        this.pending_backstroke = next_available_destroy;
                        if (this.single_backstroke_delete) {
                            return this.keydown_backstroke();
                        } else {
                            return this.pending_backstroke.addClass("search-choice-focus");
                        }
                    }
                }
            };

            Chosen.prototype.clear_backstroke = function() {
                if (this.pending_backstroke) {
                    this.pending_backstroke.removeClass("search-choice-focus");
                }
                return this.pending_backstroke = null;
            };

            Chosen.prototype.search_field_scale = function() {
                var container_width, div, style, style_block, styles, width, _i, _len;
                if (!this.is_multiple) {
                    return;
                }
                style_block = {
                    position: 'absolute',
                    left: '-1000px',
                    top: '-1000px',
                    display: 'none',
                    whiteSpace: 'pre'
                };
                styles = ['fontSize', 'fontStyle', 'fontWeight', 'fontFamily', 'lineHeight', 'textTransform', 'letterSpacing'];
                for (_i = 0, _len = styles.length; _i < _len; _i++) {
                    style = styles[_i];
                    style_block[style] = this.search_field.css(style);
                }
                div = $('<div />').css(style_block);
                div.text(this.get_search_field_value());
                $('body').append(div);
                width = div.width() + 25;
                div.remove();
                container_width = this.container.outerWidth();
                width = Math.min(container_width - 10, width);
                return this.search_field.width(width);
            };

            Chosen.prototype.trigger_form_field_change = function(extra) {
                this.form_field_jq.trigger("input", extra);
                return this.form_field_jq.trigger("change", extra);
            };

            return Chosen;

        })(AbstractChosen);

    }).call(this);

    /**
     * ezMark - A Simple Checkbox and Radio button Styling plugin.
     * This plugin allows you to use a custom Image for Checkbox or Radio button. Its very simple, small and easy to use.
     *
     * Copyright (c) Abdullah Rubiyath <http://www.itsalif.info/>.
     * Released under MIT License
     *
     * Files with this plugin:
     * - jquery.ezmark.js
     * - ezmark.css
     *
     * <usage>
     * At first, include both the css and js file at the top
     *
     * Then, simply use:
     * 	$('selector').ezMark([options]);
     *
     * [options] accepts following JSON properties:
     *  checkboxCls - custom Checkbox Class
     *  checkedCls  - checkbox Checked State's Class
     *  radioCls    - custom radiobutton Class
     *  selectedCls - radiobutton's Selected State's Class
     *
     * </usage>
     *
     * View Documention/Demo here:
     * http://www.itsalif.info/content/ezmark-jquery-checkbox-radiobutton-plugin
     *
     * @author Abdullah Rubiyath
     * @version 1.0
     * @date June 27, 2010
     */

    (function($) {
        $.fn.ezMark = function(options) {
            options = options || {};
            var defaultOpt = {
                checkboxCls   	: options.checkboxCls || 'ez-checkbox' , radioCls : options.radioCls || 'ez-radio' ,
                checkedCls 		: options.checkedCls  || 'ez-checked'  , selectedCls : options.selectedCls || 'ez-selected' ,
                hideCls  	 	: 'ez-hide'
            };
            return this.each(function() {
                var $this = $(this);
                var wrapTag = $this.attr('type') == 'checkbox' ? '<div class="'+defaultOpt.checkboxCls+'">' : '<div class="'+defaultOpt.radioCls+'">';
                // for checkbox
                if( $this.attr('type') == 'checkbox') {
                    $this.addClass(defaultOpt.hideCls).wrap(wrapTag).change(function() {
                        if( $(this).is(':checked') ) {
                            $(this).parent().addClass(defaultOpt.checkedCls);
                        }
                        else {	$(this).parent().removeClass(defaultOpt.checkedCls); 	}
                    });

                    if( $this.is(':checked') ) {
                        $this.parent().addClass(defaultOpt.checkedCls);
                    }
                }
                else if( $this.attr('type') == 'radio') {

                    $this.addClass(defaultOpt.hideCls).wrap(wrapTag).change(function() {
                        // radio button may contain groups! - so check for group
                        $('input[name="'+$(this).attr('name')+'"]').each(function() {
                            if( $(this).is(':checked') ) {
                                $(this).parent().addClass(defaultOpt.selectedCls);
                            } else {
                                $(this).parent().removeClass(defaultOpt.selectedCls);
                            }
                        });
                    });

                    if( $this.is(':checked') ) {
                        $this.parent().addClass(defaultOpt.selectedCls);
                    }
                }
            });
        }
    })(jQuery);


    /*
     * jquery-filestyle
     * doc: http://markusslima.github.io/jquery-filestyle/
     * github: https://github.com/markusslima/jquery-filestyle
     *
     * Copyright (c) 2015 Markus Vinicius da Silva Lima
     * Version 1.5.1
     * Licensed under the MIT license.
     */
    (function ($) {
        "use strict";

        var nextId = 0;

        var JFilestyle = function (element, options) {
            this.options = options;
            this.$elementjFilestyle = [];
            this.$element = $(element);
        };

        JFilestyle.prototype = {
            clear: function () {
                this.$element.val('');
                this.$elementjFilestyle.find(':text').val('');
            },

            destroy: function () {
                this.$element
                    .removeAttr('style')
                    .removeData('jfilestyle')
                    .val('');
                this.$elementjFilestyle.remove();
            },

            disabled : function(value) {
                if (value === true) {
                    if (!this.options.disabled) {
                        this.$element.attr('disabled', 'true');
                        this.$elementjFilestyle.find('label').attr('disabled', 'true');
                        this.options.disabled = true;
                    }
                } else if (value === false) {
                    if (this.options.disabled) {
                        this.$element.removeAttr('disabled');
                        this.$elementjFilestyle.find('label').removeAttr('disabled');
                        this.options.disabled = false;
                    }
                } else {
                    return this.options.disabled;
                }
            },

            buttonBefore : function(value) {
                if (value === true) {
                    if (!this.options.buttonBefore) {
                        this.options.buttonBefore = true;
                        if (this.options.input) {
                            this.$elementjFilestyle.remove();
                            this.constructor();
                            this.pushNameFiles();
                        }
                    }
                } else if (value === false) {
                    if (this.options.buttonBefore) {
                        this.options.buttonBefore = false;
                        if (this.options.input) {
                            this.$elementjFilestyle.remove();
                            this.constructor();
                            this.pushNameFiles();
                        }
                    }
                } else {
                    return this.options.buttonBefore;
                }
            },

            input: function (value) {
                if (value === true) {
                    if (!this.options.input) {
                        this.options.input = true;
                        this.$elementjFilestyle.prepend(this.htmlInput());

                        this.$elementjFilestyle.find('.count-jfilestyle').remove();

                        this.pushNameFiles();
                    }
                } else if (value === false) {
                    if (this.options.input) {
                        this.options.input = false;
                        this.$elementjFilestyle.find(':text').remove();
                        var files = this.pushNameFiles();
                        if (files.length > 0) {
                            this.$elementjFilestyle.find('label').append(' <span class="count-jfilestyle">' + files.length + '</span>');
                        }
                    }
                } else {
                    return this.options.input;
                }
            },

            buttonText: function (value) {
                if (value !== undefined) {
                    this.options.buttonText = value;
                    this.$elementjFilestyle.find('label span').html(this.options.buttonText);
                } else {
                    return this.options.buttonText;
                }
            },

            inputSize: function (value) {
                if (value !== undefined) {
                    this.options.inputSize = value;
                    this.$elementjFilestyle.find(':text').css('width', this.options.inputSize);
                } else {
                    return this.options.inputSize;
                }
            },

            placeholder : function(value) {
                if (value !== undefined) {
                    this.options.placeholder = value;
                    this.$elementjFilestyle.find(':text').attr('placeholder', value);
                } else {
                    return this.options.placeholder;
                }
            },

            htmlInput: function () {
                if (this.options.input) {
                    return '<input type="text" style="width:'+this.options.inputSize+'" placeholder="'+ this.options.placeholder +'" disabled> ';
                } else {
                    return '';
                }
            },

            // puts the name of the input files
            // return files
            pushNameFiles : function() {
                var content = '', files = [];
                if (this.$element[0].files === undefined) {
                    files[0] = {
                        'name' : this.$element.value
                    };
                } else {
                    files = this.$element[0].files;
                }

                for (var i = 0; i < files.length; i++) {
                    content += files[i].name.split("\\").pop() + ', ';
                }

                if (content !== '') {
                    this.$elementjFilestyle.find(':text').val(content.replace(/\, $/g, ''));
                } else {
                    this.$elementjFilestyle.find(':text').val('');
                }

                return files;
            },

            constructor: function () {
                var _self = this,
                    html = '',
                    id = _self.$element.attr('id'),
                    $label,
                    files = [];

                if (id === '' || !id) {
                    id = 'jfilestyle-' + nextId;
                    _self.$element.attr({'id': id});
                    nextId++;
                }

                html = '<span class="focus-jfilestyle">'+
                    '<label for="'+id+'" ' + (_self.options.disabled ? 'disabled="true"' : '') + '>'+
                    '<span>'+_self.options.buttonText+'</span>'+
                    '</label>'+
                    '</span>';

                if (_self.options.buttonBefore === true) {
                    html = html + _self.htmlInput();
                } else {
                    html = _self.htmlInput() + html;
                }

                _self.$elementjFilestyle = $('<div class="jfilestyle ' + (_self.options.input?'jfilestyle-corner':'') + ' ' + (this.options.buttonBefore ? ' jfilestyle-buttonbefore' : '') + '">'+html+'</div>');
                _self.$elementjFilestyle.find('.focus-jfilestyle')
                    .attr('tabindex', "0")
                    .keypress(function (e) {
                        if (e.keyCode === 13 || e.charCode === 32) {
                            _self.$elementjFilestyle.find('label').click();
                            return false;
                        }
                    });

                // hidding input file and add filestyle
                _self.$element
                    .css({'position': 'absolute', 'clip': 'rect(0px 0px 0px 0px)'})
                    .attr('tabindex', "-1")
                    .after(_self.$elementjFilestyle);

                if (_self.options.disabled) {
                    _self.$element.attr('disabled', 'true');
                }

                // Getting input file value
                _self.$element.change(function () {
                    var files = _self.pushNameFiles();

                    if (_self.options.input == false) {
                        if (_self.$elementjFilestyle.find('.count-jfilestyle').length == 0) {
                            _self.$elementjFilestyle.find('label').append(' <span class="count-jfilestyle">' + files.length + '</span>');
                        } else if (files.length == 0) {
                            _self.$elementjFilestyle.find('.count-jfilestyle').remove();
                        } else {
                            _self.$elementjFilestyle.find('.count-jfilestyle').html(files.length);
                        }
                    } else {
                        _self.$elementjFilestyle.find('.count-jfilestyle').remove();
                    }
                });

                // Check if browser is Firefox
                if (window.navigator.userAgent.search(/firefox/i) > -1) {
                    // Simulating choose file for firefox
                    this.$elementjFilestyle.find('label').click(function () {
                        _self.$element.click();
                        return false;
                    });
                }
            }
        };

        var old = $.fn.jfilestyle;

        $.fn.jfilestyle = function (option, value) {
            var get = '',
                element = this.each(function () {
                    if ($(this).attr('type') === 'file') {
                        var $this = $(this),
                            data = $this.data('jfilestyle'),
                            options = $.extend({}, $.fn.jfilestyle.defaults, option, typeof option === 'object' && option);

                        if (!data) {
                            $this.data('jfilestyle', (data = new JFilestyle(this, options)));
                            data.constructor();
                        }

                        if (typeof option === 'string') {
                            get = data[option](value);
                        }
                    }
                });

            if (typeof get !== undefined) {
                return get;
            } else {
                return element;
            }
        };

        $.fn.jfilestyle.defaults = {
            'buttonText': 'Choose file',
            'input': true,
            'disabled': false,
            'buttonBefore': false,
            'inputSize': '200px',
            'placeholder': ''
        };

        $.fn.jfilestyle.noConflict = function () {
            $.fn.jfilestyle = old;
            return this;
        };

        $(function() {
            // Data attributes register
            $('.jfilestyle').each(function () {
                var $this = $(this),
                    options = {
                        'buttonText': $this.attr('data-buttonText'),
                        'input': $this.attr('data-input') === 'false' ? false : true,
                        'disabled': $this.attr('data-disabled') === 'true' ? true : false,
                        'buttonBefore': $this.attr('data-buttonBefore') === 'true' ? true : false,
                        'inputSize': $this.attr('data-inputSize'),
                        'placeholder': $this.attr('data-placeholder')
                    };

                $this.jfilestyle(options);
            });
        });
    })(window.jQuery);



    jQuery(document).ready(function($){
//$( window ).load(function() {

        console.log('ready');



        $('#invia').click(function (event) {
            event.preventDefault();
        });


        $('#form1').each(function(index,item) {
            // Create a new element and assign it attributes from the current element
            var NewElement = $("<form />");
            $.each(this.attributes, function(i, attrib){
                $(NewElement).attr(attrib.name, attrib.value);
            });

            // Replace the current element with the new one and carry over the contents
            $(item).replaceWith(function () {
                return $(NewElement).append($(this).contents());
            });
        });

        $('#form1').attr('method',"post");
        $('#form1').attr('enctype',"multipart/form-data");
        $('#form1').attr('novalidate',"");


        console.log($("#form1"));


        $('#data-di-nascita-gg,#data-di-nascita-mm,#data-di-nascita-aaaa').on("change paste keyup blur", function() {
            /* YYYY-MM-GG */
            var dataDiNascitaAAAA = $('#data-di-nascita-aaaa').val();
            var dataDiNascitaMM = $('#data-di-nascita-mm').val();
            var dataDiNascitaGG = $('#data-di-nascita-gg').val();

            console.log(dataDiNascitaAAAA);
            console.log(dataDiNascitaMM);
            console.log(dataDiNascitaGG);

            if(
                (dataDiNascitaAAAA!=undefined && dataDiNascitaAAAA!='' && dataDiNascitaAAAA.length===4)
                && (dataDiNascitaMM!=undefined && dataDiNascitaMM!='' && dataDiNascitaMM.length===2)
                && (dataDiNascitaGG!=undefined && dataDiNascitaGG!='' && dataDiNascitaGG.length===2)
            )
            {
                var dataDiNascita = dataDiNascitaAAAA+'-'+dataDiNascitaMM+'-'+dataDiNascitaGG;
                console.log(dataDiNascita);
                $('#data-di-nascita').val(dataDiNascita);
            }
            else
            {
                $('#data-di-nascita').val('');
            }

        });

        $('#data-erogazione-scontrino-gg,#data-erogazione-scontrino-mm,#data-erogazione-scontrino-aaaa').on("change paste keyup blur", function() {
            /* YYYY-MM-GG */
            var dataErogazioneScontrinoAAAA = $('#data-erogazione-scontrino-aaaa').val();
            var dataErogazioneScontrinoMM = $('#data-erogazione-scontrino-mm').val();
            var dataErogazioneScontrinoGG = $('#data-erogazione-scontrino-gg').val();

            console.log(dataErogazioneScontrinoAAAA);
            console.log(dataErogazioneScontrinoMM);
            console.log(dataErogazioneScontrinoGG);

            if(
                (dataErogazioneScontrinoAAAA!=undefined && dataErogazioneScontrinoAAAA!='' && dataErogazioneScontrinoAAAA.length===4)
                && (dataErogazioneScontrinoMM!=undefined && dataErogazioneScontrinoMM!='' && dataErogazioneScontrinoMM.length===2)
                && (dataErogazioneScontrinoGG!=undefined && dataErogazioneScontrinoGG!='' && dataErogazioneScontrinoGG.length===2)
            )
            {
                var dataErogazioneScontrino = dataErogazioneScontrinoAAAA+'-'+dataErogazioneScontrinoMM+'-'+dataErogazioneScontrinoGG;
                console.log(dataErogazioneScontrino);
                $('#data-erogazione-scontrino').val(dataErogazioneScontrino);
            }
            else
            {
                $('#data-erogazione-scontrino').val('');
            }

        });

        $('#ora-erogazione-scontrino-hh,#ora-erogazione-scontrino-mm').on("change paste keyup blur", function() {
            /* OO:MM:SS */
            var oraErogazioneScontrinoHH = $('#ora-erogazione-scontrino-hh').val();
            var oraErogazioneScontrinoMM = $('#ora-erogazione-scontrino-mm').val();

            console.log(oraErogazioneScontrinoHH);
            console.log(oraErogazioneScontrinoMM);

            if(
                (oraErogazioneScontrinoHH!=undefined && oraErogazioneScontrinoHH!='' && oraErogazioneScontrinoHH.length===2)
                && (oraErogazioneScontrinoMM!=undefined && oraErogazioneScontrinoMM!='' && oraErogazioneScontrinoMM.length===2)
            )
            {
                var oraErogazioneScontrino = oraErogazioneScontrinoHH+':'+oraErogazioneScontrinoMM+':00';
                console.log(oraErogazioneScontrino);
                $('#ora-erogazione-scontrino').val(oraErogazioneScontrino);
            }
            else
            {
                $('#ora-erogazione-scontrino').val('');
            }

        });

        $('#totale-spesa-intera,#totale-spesa-decimale').on("change paste keyup blur", function() {

            var totaleSpesaIntera = $('#totale-spesa-intera').val();
            var totaleSpesaDecimale = $('#totale-spesa-decimale').val();

            console.log(totaleSpesaIntera);
            console.log(totaleSpesaDecimale);

            if(
                (totaleSpesaIntera!=undefined && totaleSpesaIntera!='')
                && (totaleSpesaDecimale!=undefined && totaleSpesaDecimale!='')
            )
            {
                var totaleSpesa = totaleSpesaIntera+'.'+totaleSpesaDecimale;
                console.log(totaleSpesa);
                $('#totale-spesa').val(totaleSpesa);
            }
            else
            {
                $('#totale-spesa').val('');
            }

        });





        $("#form1 select").chosen({disable_search: true});

        $( window ).resize(function() {
            $("#form1 select").chosen( "destroy" );
            $("#form1 select").chosen({disable_search: true});
        });

        $('#form1 input[type="radio"]').ezMark();

        $('#form1 .open-info').click(function(event){
            event.preventDefault();
            $($(this).attr('href')).addClass('active');
        });
        $('#form1 .close-info').click(function(event){
            event.preventDefault();
            $($(this).attr('href')).removeClass('active');
        });

        var ok = function(){
            var serializedForm = $('#form1').serializeArray();
            console.log(serializedForm);
            $('#form1').removeClass('with-errors');
            $('#formErrors').html('');
            var htmlForm = '<h2>Grazie per aver partecipato al concorso</h2>';
            htmlForm += '<p class="p-end">Se la registrazione dei dati e quanto caricato sarà conforme a quanto richiesto nel regolamento, riceverai entro 180 giorni dalla data di registrazione il premio all’indirizzo indicato.</p>';
            //htmlForm += '<div class="info-end"><ul><li>l’originale dello scontrino attestante l’acquisto dei prodotti promozionati</li><li>copia del modulo precedentemente scaricato e debitamente compilato a: <br>&nbsp;&nbsp;&nbsp;&nbsp;Operazione “FRANCK PROVOST TI REGALA UNA BORSA GONFIABILE DA MARE TOO LATE”<br>&nbsp;&nbsp;&nbsp;&nbsp;Viale A. Volta 60, 20090 CUSAGO (MI)</li></ul><a href="javascript:window.print();" class="but_print">STAMPA IL MODULO DI RIEPILOGO</a></div>';


            var notToPrint = ['data-di-nascita-gg','data-di-nascita-mm','data-di-nascita-aaaa','data-erogazione-scontrino-gg','data-erogazione-scontrino-mm','data-erogazione-scontrino-aaaa','ora-erogazione-scontrino-hh','ora-erogazione-scontrino-mm','totale-spesa-intera','totale-spesa-decimale','informativa','marketing','profilazione','g-recaptcha-response'];

            htmlForm += '<div class="serialized-form">';
            [].forEach.call(serializedForm,function(item){

                var name = item.name.trim().toLowerCase();

                var toPrint = (notToPrint.indexOf(name) === -1) ? true : false;

                console.log(name+' toPrint: '+toPrint);

                //console.log(notToPrint.indexOf(item.name));
                if(toPrint){
                    htmlForm += name+': '+item.value+'<br>';
                }
            });
            htmlForm += '</div>';

            $('#form1').html(htmlForm);
        };

        //ok();



        form1Submit = function(){
            //alert('submit');
            console.log('submit');


            var files = {};

            $('#form1 input[type="file"]').each(function() {
                if($(this).val() === "") {
                    var lastClass = $(this).parent().prop('class');
                    lastClass = lastClass.split(' ')[lastClass.split(' ').length - 1];
                    console.log(lastClass);
                    files[lastClass] = $(this);
                    $(this).detach();
                }
            });
            console.log('Files detached');
            console.log(files);

            var formData = new FormData($("#form1")[0]);

            console.log('reattach files');
            $.each( files, function( key, value ) {
                $('#form1 '+'.'+key).prepend(value);
            });

            //$( "#sended" ).html( 'SENDED: '+JSON.stringify($("#form1").serializeArray()) );

            //console.log(formData);



            $.ajax({
                url: 'https://concorsi-franckprovost.diginventa.it/api/garnier/12',
                type: 'POST',
                data: formData,
                async: false,
                success: function (data) {
                    // console.log("Success: Files sent!");

                    console.log(data);
                    //$( "#results" ).html( 'RESULTS: '+JSON.stringify(data) );

                    /*
                     Error
                     variable of type int, 0 for no errors and 1 if there are errors
                     Error Codes
                     Type Array that may contain one or more of below codes
                     Code	Message
                     1	concorso doesn't exist
                     2	concorso start date is future(didnt started yet)
                     3	concorso end date is past(ended already)
                     4	Missing fields or mismatch of field types
                     5	user limit reached
                     6	user daily limit reached
                     7	user weekly limit reached
                     8	user monthly limit reached
                     9	user yearly limit reached
                     10	uploaded file format is not accepted
                     11	Non ajax call
                     12	Captcha not valid
                     13	Image size greater than 3MB
                     14	Image dimensions greater than 400x400
                     15	Email not valid
                     Missing
                     Missing is an array that contains arrays which indicates missing field and its type
                     typeError
                     typeError is an array that contains arrays which indicates mismatch field and its correct type
                     Example of json response
                     {"error":1,"errorCodes":[4],"missing":[["email","text"],["name","text"]],"typeError":[["foto","foto"],["city","text"]]}
                     */

                    $('#form1').removeClass('with-errors');
                    $('.form-item').removeClass('with-error');
                    $('#formErrors').html('');

                    if( data.error==0 )
                    {
                        ok();
                    }
                    else
                    {
                        var errori=[];
                        //console.log(data['errorCodes'][0]);
                        switch(data['errorCodes'][0]){
                            case 1:
                                errori.push('Il concorso non esiste');
                                break;
                            case 2:
                                errori.push('Il concorso non è ancora iniziato');
                                break;
                            case 3:
                                errori.push('Il concorso è concluso');
                                break;
                            case 4:
                                errori.push('Mancano dei campi o non sono stati compilati correttamente');
                                for(var i=0;i<data['missing'].length;i++)
                                {
                                    var missing = data['missing'][i];
                                    $('.form-item-'+data['missing'][i]).addClass('with-error');
                                }
                                break;
                            case 5:
                                errori.push('Raggiunto il limite per utente');
                                break;
                            case 6:
                                errori.push('Raggiunto il limite giornaliero per utente');
                                break;
                            case 7:
                                errori.push('Raggiunto il limite settimanale per utente');
                                break;
                            case 8:
                                errori.push('Raggiunto il limite mensile per utente');
                                break;
                            case 9:
                                errori.push('Raggiunto il limite annuale per utente');
                                break;
                            case 10:
                                errori.push('Formato del file non corretto');
                                break;
                            case 11:
                                errori.push('Chiamata non ajax');
                                break;
                            case 12:
                                errori.push('CAPTCHA non valido');
                                break;
                            case 13:
                                errori.push("Le immagini caricate non rispettano i requisiti richiesti");
                                break;
                            case 14:
                                errori.push("L'immagine deve essere almeno 400x400 px");
                                break;
                            case 15:
                                errori.push('Email non valida');
                                break;
                            case 16:
                                errori.push("La registrazione dello scontrino deve esser effettuata entro 24 h dall'acquisto.");
                                break;
                            case 17:
                                errori.push('La data dello scontrino non può essere una data futura.');
                                break;
                            case 18:
                                errori.push('La data dello scontrino è scaduta.');
                                break;

                        }
                        console.log(errori);
                        $('#form1').addClass('with-errors');
                        $('#formErrors').append('<p>'+errori.join('<br>')+'</p>');
                        grecaptcha.reset();
                    }




                },
                cache: false,
                contentType: false,
                processData: false
            });
        };

        $("#invia").click(function(event){
            event.preventDefault();
            form1Submit();
        });



    });




};


var timeoutForm = window.setTimeout(initForm, 1000);
