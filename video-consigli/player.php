<?php 
 /*******************************************************************************************
 * il player supporta il formato MP4 con codec video H.264 e codec audio AAC
 * http://www.longtailvideo.com/support/jw-player/28836/media-format-support/
 *******************************************************************************************/ 

	require_once("functions.php");
?>

<?php if (standalone()): ?>
<!DOCTYPE html>
<html lang="it">
<head>
<meta charset="utf-8" />
<style type="text/css">
	#player{
	text-align: center;
	line-height: 200px;
}
</style>
</head>
<body>
<?php endif; ?>

<script type="text/javascript" src="../js/videoplayer/swfobject.js"></script>
<script type="text/javascript">

var flashvars = {
	file:"<?php echo(path()) ?>",
	skin:"../js/videoplayer/skin.zip",
	autostart:"true"
	};
var params = {
  allowfullscreen:"true",
  allowscriptaccess:"always",
  no_flash: 'Sorry, you need to install flash to see this content.'  
};
var attributes = {
  id: "player",
  name: "player"
};

swfobject.embedSWF("../js/videoplayer/player.swf", "player", "<?php echo(width()) ?>", "<?php echo(height()) ?>", "9.0.0","expressInstall.swf", flashvars, params, attributes);

</script>

<div id="player">Installa flash player</div>

<?php if (standalone()): ?>
</body></html>
<?php endif; ?>