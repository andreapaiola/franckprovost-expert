<?php 

//functions
function standalone() {
	if (isset($_GET['solo']) && $_GET['solo'] == "1"){ return true;}
	return false;
} //standalone
function get_video_id() {
	global $video;
	if (isset($_GET['video'])) {$id_video = abs((int)($_GET['video'])); } else return 3;
	if ( (1 <= $id_video) && ($id_video <= count($video)) ) { return $id_video; } else return 3;
} //get_video_number
function path() {
	global $video;
	return "../../video/".$video[(get_video_id()-1)]["path"];
} //path
function width() {
	global $video;
	return $video[(get_video_id()-1)]["width"];
} //width
function height() {
	global $video;
	return $video[(get_video_id()-1)]["height"];
} //height

//data
$video = array(	
	array(  "path" => "colorazione-spot.mp4", "width" => "418", "height" => "292" ),
	array(  "path" => "01.mp4", "width" => "418", "height" => "292" ),
	array(  "path" => "02.mp4", "width" => "418", "height" => "292" ),
	array(  "path" => "03.mp4", "width" => "418", "height" => "292" ),
	array(  "path" => "05.mp4", "width" => "418", "height" => "292" )
);

?>