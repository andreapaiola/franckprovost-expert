</div> <!-- marchio -->
<div id="footer_inside">
	<div class="footer_menu">
		<div class="left">*Parrucchiere N&deg;1 per numero di saloni presenti sul territorio</div>
		<ul>
			<li><a rel="shadowbox;width=350;height=400" class="option" title="Informazioni legali" href="../FP_Privacy-Policy_GDPR-2018.pdf" target="_blank">Privacy policy</a></li>
			<li><a class="option" title="Cookie policy" href="../FP_Cookie-Policy_GDPR-2018.pdf" target="_blank">Cookie policy</a></li><li><a class="option" title="Termini di utilizzo" href="/Termini-Utilizzo-Sito-FRANK-PROVOST.pdf" target="_blank">Termini di utilizzo</a></li>
			<li><a title="Concorsi" href="../concorsi.htm">Operazioni a premio</a></li><li><a rel="shadowbox;width=350;height=165" class="option" title="Contatti" href="../contatti-box.htm" id="no_border">Contatti</a></li>
		</ul>
	</div> <!--footer_menu-->
</div> <!--footer_inside-->

</div>

<script type="text/javascript" src="../js/app.js"></script>
<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-22773381-1']);
	_gaq.push (['_gat._anonymizeIp']);
	_gaq.push(['_trackPageview']);

(function() {
	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
</script>

</body>
</html>
