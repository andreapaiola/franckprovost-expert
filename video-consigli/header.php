<!DOCTYPE html>
<html lang="it">
<head>
<meta charset="utf-8" />
<title>FRANCK PROVOST</title>
<meta name="description" content="Franck Provost Expert - La gamma professionale dal formato salone ad un prezzo convenienza" />
<meta name="keywords" content="franck provost, franckprovost, franck provost expert, franc provo, frank provost, prov&ograve;, prov&ograve; shampoo, provo shampoo, provost shampoo, provost gamma, franck provost shampoo, balsamo, shampoo professionale, balsamo professionale, trattamento per capelli, expert capelli, capelli brillanti, expert brillance, colorazione per capelli, esperto colorazione, capelli colorati, expert couleur, capelli nutriti, expert nutrition, capelli riparati, expert reparation, capelli lisci, expert lissage" />
<link href="../css/style.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>

<!-- Shadowbox -->
<link rel="stylesheet" type="text/css" href="../js/shadowbox-3.0.3/shadowbox.css">
<script type="text/javascript" src="../js/shadowbox-3.0.3/shadowbox.js"></script>
<script type="text/javascript">
	Shadowbox.init({	displayCounter:	false});
</script>

<!--dropdown-->
<script type="text/javascript" src="../js/dropdown/bootstrap-dropdown.js"></script>
<link rel="stylesheet" type="text/css" href="../js/dropdown/dropdown-menu.css">
</head>

<body>
	<div class="cookie-policy">
		<div class="row">
			<h3>Informativa Cookie</h3>
			<p>
			   Questo sito utilizza cookie, anche di terze parti, per inviarti pubblicità e servizi in linea con le tue preferenze. 
			   Se vuoi saperne di più o negare il consenso a tutti o ad alcuni cookie <a href="cookie-policy.html">clicca qui</a>. <br/>
Chiudendo questo banner, scorrendo questa pagina o cliccando qualunque suo elemento, acconsenti all'uso dei cookie.
			</p>
			<button>ok</button>
		</div>
	</div>
	<div class="wrapper" id="inside">
		<div class="marchio">
			<div class="header_cont">
				<div class="header">
					<div class="logo"><img src="../img/img_logo.png" alt="Logo" title="Logo" /></div>
					<ul class="menu">
						<li><a href="../default.htm" >Homepage</a></li>
						<li><a href="../marchio.htm">La Marca</a></li>					
						<li><a href="../consigli.htm">I miei consigli</a></li>
						<li class="dropdown" ><a  class="dropdown-toggle" data-toggle="dropdown" href="#menu1">La Gamma</a>
							<ul class="dropdown-menu">
								<li><a href="../gamma-colorazione.htm">Colorazione</a></li>
								<li><a href="../gamma_couleur.htm">Hair Care</a></li>
								<li><a href="../gamma-styling-mousse.htm">Styling</a></li>
							</ul>
						</li>
						<li><a id="evid" href="video.php">Video Consigli</a></li>	
					</ul>
				</div> <!--header-->
			</div> <!--header_cont-->
		
			<div id="navbar">
				<p><a href="../default.htm">Homepage</a> &raquo; <a href="video.php">VIDEO CONSIGLI</a> </p>
			</div> <!--navbar-->